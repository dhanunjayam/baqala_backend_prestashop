<?php 
include("dbdata.php");
include("msgcode.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);
//$allproducts=array();
if(!$_GET['subCategoryId']) {
  $result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  echo json_encode($result);
  exit(0);
}
$sub_categoryId=$_GET['subCategoryId'];
if(!$conn)
{
  $result=array('result'=>array('status'=>0,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  echo json_encode($result);
}
 //select all products for a particular category
else {
   $sql='SELECT id_product FROM ps_product where id_category_default='.$sub_categoryId;
   $res1=mysql_query($sql);
   echo json_encode($res1);
   //$attribute =array();
   $num_rows=mysql_num_rows($res1);
    if($num_rows==0) {
          $result=array('result'=>array('status'=>0,'message'=>$msgcode[109],'version'=>"1.0",'msgcode'=>109));
          echo json_encode($result);
    }
    else {
        $products = array();
        while($row = mysql_fetch_assoc($res1)) {
          $product['id']=$row['id_product'];
          //query to fetch name of the product
          $sql='SELECT name,description FROM ps_product_lang WHERE id_product='.$product['id'];
          $res=mysql_query($sql);
          $row=mysql_fetch_assoc($res);
          $product['name']=$row['name'];
            $product['description']=ltrim($row['description'],"<p>");
            $product['description']=rtrim($product['description'],"</p>");
          /*query to check whether varience exist for a product, every product will have a varience attached to it, If it is more than one 
          that indicates the product has varience attached to it*/
          $sql='SELECT * FROM ps_product_attribute WHERE id_product='.$product['id'];
           $res=mysql_query($sql);
           $num_rows=mysql_num_rows($res);
      
             if($num_rows<1)//to fetch product details without any varience
              {
                 $product['variance']="no";
                 $sql=mysql_query('SELECT quantity FROM ps_stock_available where id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $product['stock']=$row['quantity'];

                 $sql=mysql_query('SELECT price FROM ps_product where id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $product['actualPrice']=$row['price'];

                 $sql=mysql_query('SELECT reduction FROM ps_specific_price where id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $discountAmount=$row['reduction'];
                 if($discountAmount>0)
                    //$product['discountPrice']=$product['actualPrice']-$discountAmount;
                     $product['discountPrice']="BD ".($product['actualPrice']-$discountAmount);
                 else
                    $product['discountPrice']="";
                 $product['actualPrice']="BD ".$product['actualPrice'];
                 //id_feature=4 for weight
                 $sql=mysql_query('SELECT id_feature_value FROM `ps_feature_product` where id_feature=4 and id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $id_feature=$row['id_feature_value'];

                 $sql=mysql_query('SELECT value FROM `ps_feature_value_lang` where id_feature_value='.$id_feature);
                 $row=mysql_fetch_assoc($sql);
                 $product['measure']=$row['value']; 
                 //will fetch the url of default image of the product
                 //$product['image']="http://192.168.1.41/prestashop/img/p/";
                 $sql = mysql_query('SELECT id_image FROM ps_image where id_product='.$product['id'].' and cover=1'); 
                 $num_rows=mysql_num_rows($sql);
                 if($num_rows==0) {
                    $product['image']="http://192.168.1.41/prestashop/img/p/en.jpg";
                   } 
                else
                {    
                  $product['image']="http://192.168.1.41/prestashop/img/p/";          
                  while($row=mysql_fetch_assoc($sql)) 
                 {
                      $arr = str_split($row['id_image'],1);
                      foreach($arr as $a)
                      {
                        $product['image']=$product['image'].$a.'/';
                      }
                      $product['image']=$product['image'].$row['id_image'].'.jpg';
                  }
               }// fetched product details without any varience
               array_push($products,$product);//print_r($products);
               $product=[];
              }

             else //fetch product varience details
                {
                 $product['variance']="yes";
                 $sql='SELECT id_product_attribute FROM ps_product_attribute where default_on=1 and id_product='.$product['id'];
                 $res=mysql_query($sql);
                 $row=mysql_fetch_assoc($res);
                 $attribute_id=$row['id_product_attribute'];// $attribute_id has default product variance id
                 $sql=mysql_query('SELECT quantity FROM ps_stock_available where id_product='.$product['id'].' and id_product_attribute='.$attribute_id);
                 $row=mysql_fetch_assoc($sql);
                 $product['stock']=$row['quantity'];
                 $sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$attribute_id.' ORDER BY id_image DESC LIMIT 1');
                 //$sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$attribute_id);
                 $num_rows=mysql_num_rows($sql);
                 if($num_rows==0) 
                    {
                    $product['image']="http://192.168.1.41/prestashop/img/p/en.jpg";
                     } 
                 else{  
                   $product['image']="http://192.168.1.41/prestashop/img/p/";
                   while($row=mysql_fetch_assoc($sql))
                    {
                      $arr = str_split($row['id_image'],1);
                      foreach($arr as $a)
                      {
                        $product['image']=$product['image'].$a.'/';
                      }
                      $product['image']=$product['image'].$row['id_image'].'.jpg';
                    }
                  } 
                  
                  $sql='SELECT id_attribute FROM ps_product_attribute_combination where id_product_attribute='.$attribute_id;
                  $res_attributes=mysql_query($sql);
                  while($row2=mysql_fetch_assoc($res_attributes))
                     {
                      $id_attribute=$row2['id_attribute'];
                      $sql='SELECT id_attribute_group FROM ps_attribute where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $id_attribute_group=$row['id_attribute_group'];
                      $sql='SELECT name FROM ps_attribute_group_lang where id_attribute_group='.$id_attribute_group;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $attri_name=$row['name'];
                      $sql='SELECT name FROM `ps_attribute_lang` where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $product[$attri_name]=$row['name'];
                      }

                  // query to select all the product variance (attribute) 
                 $sql='SELECT id_product_attribute FROM ps_product_attribute where id_product='.$product['id'];
                 $res_at_id=mysql_query($sql);
                 $variants=array();
                 while($row1=mysql_fetch_assoc($res_at_id))
                 {
                   $variant['attributeId']=$row1['id_product_attribute'];
                 /*based on the  product attribute id, we will fetch quantity for the product*/
                   $sql=mysql_query('SELECT quantity FROM ps_stock_available where id_product='.$product['id'].' and id_product_attribute='.$variant['attributeId']);
                   $row=mysql_fetch_assoc($sql);
                   $variant['stock']=$row['quantity'];
                   $sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant['attributeId'].' ORDER BY id_image DESC LIMIT 1');
                   //$sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant['attribute_id']);

                   $num_rows=mysql_num_rows($sql);
                   if($num_rows==0) 
                   {
                      $variant['image']="http://192.168.1.41/prestashop/img/p/en.jpg";
                     } 
                else{  
                   $variant['image']="http://192.168.1.41/prestashop/img/p/";
                   while($row=mysql_fetch_assoc($sql))
                    {
                      $arr = str_split($row['id_image'],1);
                      foreach($arr as $a)
                      {
                        $variant['image']=$variant['image'].$a.'/';
                      }
                      $variant['image']=$variant['image'].$row['id_image'].'.jpg';
                    }
                  } 
                   $sql='SELECT id_attribute FROM ps_product_attribute_combination where id_product_attribute='.$variant['attributeId'];
                   $res_attributes=mysql_query($sql);

                   while($row2=mysql_fetch_assoc($res_attributes))
                     {
                      $id_attribute=$row2['id_attribute'];
                      $sql='SELECT id_attribute_group FROM ps_attribute where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $id_attribute_group=$row['id_attribute_group'];
                      $sql='SELECT name FROM ps_attribute_group_lang where id_attribute_group='.$id_attribute_group;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $attri_name=$row['name'];

                      if ($row['name'] == 'measure') {
                        $attribute_name = 'measure';
                      }
                      else{
                        $attribute_name = '';
                      }
                      $sql='SELECT name FROM `ps_attribute_lang` where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $variant[$attri_name]=$row['name'];

                      if ($attribute_name == 'measure') {
                        $sep_str = explode(" ", $row['name']);
                       // echo $sep_str[1], PHP_EOL;
                        if (strtolower($sep_str[1]) == "kg") {
                          $gm_value = $sep_str[0] * 1000; 
                         // echo $gm_value, PHP_EOL;
                          $variant['convertedMeasure'] = $gm_value;
                        }
                        else if(strtolower($sep_str[1]) == "ltr"){
                           $ml_value = $sep_str[0] * 1000; 
                         // echo $ml_value, PHP_EOL;
                          $variant['convertedMeasure'] = $ml_value;
                        }
                        else{
                          $variant['convertedMeasure'] = $sep_str[0];
                        }

                      }

                      }
                     // if(!$variant['discountPrice']){
                      //  $variant['discountPrice']='';
                      if(!$variant['discountPrice']){
                        $variant['discountPrice']='';
                      }
                    else 
                    {
                        //if(substr($variant['discountPrice'],0,3)!=" BD")
                        $variant['discountPrice']="BD ".$variant['discountPrice'];
                     }
                        
                    //if(substr($variant['actualPrice'],0,3)!=" BD")
                        $variant['actualPrice']="BD ".$variant['actualPrice'];
                      
                                      // echo $variant[$attri_name], PHP_EOL;

                    array_push($variants,$variant);
                    $variant=[];
                 }

                  function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
                    $sort_col = array();
                    foreach ($arr as $key=> $row) {
                        $sort_col[$key] = $row[$col];
                    }

                    array_multisort($sort_col, $dir, $arr);
                  }

                 array_sort_by_column($variants, 'convertedMeasure');


                  // foreach($variants as $tempArr) {

                  //     $str = $tempArr['convertedMeasure'];

                  //     echo $str, PHP_EOL;
                  // }

              
                    $product=array(
                    //'product'=>$product,
                    'id'=>$product['id'],
                    'name'=>$product['name'],
                    'description'=>$product['description'],
                    'image'=>$product['image'],
                    'variance'=>$product['variance'],
                    'stock'=>$product['stock'],
                    'measure'=>$product['measure'],
                    'actualPrice'=>"BD ".$product['actualPrice'],
                    'discountPrice'=>$product['discountPrice']==null?'':"BD ".$product['discountPrice'],
                    'variantsList'=>$variants
                    );
                  array_push($products,$product);
                  $product = [];
              }//fetched product varience details 
        }//each product detail along with its varient
     $result=array('result'=> array('status' => 1,'message' =>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'subCategoryId'=>$sub_categoryId,'products'=>$products);
     echo json_encode($result);  
  }
}
?>