<?php 
include("dbdata.php");
include("msgcode.php");
include_once("baqala-utils.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);

if(!$conn)
{
	$result=array('result'=>array('status'=>0,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  	echo json_encode($result);
}
else 
{
	if(!$_GET['accessToken']) 
	{
  		$result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  		echo json_encode($result);
  		exit(0);
	}
	else
	{
		$accessToken=$_GET['accessToken'];
		$sql_custId=mysql_query("SELECT id_customer from ps_customer where access_token = '$accessToken' ");
		
		$num=mysql_num_rows($sql_custId);
		if($num==0)
		{
			$result=array('result'=>array('status'=>0,'message'=>$msgcode[117],'version'=>"1.0",'msgcode'=>117));
  			echo json_encode($result);	
		}
		else
		{
			$res=mysql_fetch_assoc($sql_custId);
			$custId=$res['id_customer'];
			
			$sql_userdetails=mysql_query("SELECT firstname,lastname,email,phone_num,location FROM `ps_customer` where id_customer=$custId");
			$customer=array();
			while($row=mysql_fetch_assoc($sql_userdetails))
			{
				$cust['firstName']=$row['firstname'];
				$cust['lastName']=$row['lastname'];
				$cust['email']=$row['email'];
				//$res=substr($cust['email'], -12);
				//emailId ending with  bakkala and baqala were appended to phone number when user doesnot provide email details so should not display them
				//if($res === "@bakkala.com" || "@baqala.com")
				if(endsWith($cust['email'], "@baqala.com") || endsWith($cust['email'], "@bakkala.com"))
						$cust['email']="";
				$cust['phNo']=$row['phone_num'];
				$cust['location']=$row['location'];
				array_push($customer,$cust);
			}
			$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'userDetails'=>$customer);
  			echo json_encode($result);	

		}
	}
}