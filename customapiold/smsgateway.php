<?php
require ("gatewaydetails.php");
function send_sms($phonenumber, $otp, $time)
{
	global $gateway_username;
	global $gateway_password;
$sender="baqala";
$msg_text="One time password is ".$otp." for Baqala. This OTP is valid for ".$time;
$dsc=0; //English language binary=0
$url ="https://www.bahrainsms.net/api/?username=".$gateway_username."&password=". $gateway_password."&sms=".$msg_text."&mobile=".$phonenumber."&binary=".$dsc;
$url = str_replace(' ', '%20', $url);
$status=file_get_contents($url);	
$status=substr($status,70);

$staus_str =  getMessageFromStatus(str_replace("\n","",$status));

return $staus_str;
}

function getMessageFromStatus($message)
{
	$messageStr = "";
	switch ($message) {
		case 'NOT_AUTHORIZED':
			$messageStr = "Invalid username and/or password ";
			break;
		case 'NO_CREDIT':
			$messageStr = "Not enough credits in user account ";
			break;
		case 'NO_RECEIPENTS':
			$messageStr = "No recipients";
			break;
		case 'INVALID_RECEIPENTS':
			$messageStr = "No good recipients";
			break;
		case 'NO_TEXT':
			$messageStr = "Empty sms text";
			break;
		case 'NO_BINARY':
			$messageStr = "Language flag not set";
			break;
		case 'NO_SENDER':
			$messageStr = "No sender ID";
			break;
		case 'INVALID_SENDER':
			$messageStr = "Invalid sender id ( more than 11 characters )";
			break;
		default:
			$messageStr = "Request accepted";
			break;
	}

	return $messageStr;
}
?>