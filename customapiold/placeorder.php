<?php 
include_once("dbdata.php");
include_once("msgcode.php");
include_once("carttotal.php");
include_once("defaultTimeZone.php");
//error_reporting(E_ALL);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);
if(!$conn)
{
	$result=array('result'=>array('status'=>0,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  	echo json_encode($result);
}
else 
{
	if((!$_GET['accessToken']) || (!$_GET['timeSlotId']) || (!$_GET['addressId']) || (!$_GET['selectedDate']))
	{
  		$result=array('result'=>array('status'=>0,'message'=>$msgcode[134],'version'=>"1.0",'msgcode'=>134));
  		echo json_encode($result);
  		exit(0);
	}
	else
	{
		$accessToken=$_GET['accessToken'];
		$sql_custId=mysql_query("SELECT id_customer,location from ps_customer where access_token = '$accessToken' ");
		$num=mysql_num_rows($sql_custId);
		if($num==0)
		{
			$result=array('result'=>array('status'=>0,'message'=>$msgcode[117],'version'=>"1.0",'msgcode'=>117));
  			echo json_encode($result);	
        exit(0);
		}
		else
		{

      $selectedDate = $_GET['selectedDate'];

			$addressId=$_GET['addressId'];
			$timeslot=$_GET['timeSlotId'];
			$res_custId=mysql_fetch_assoc($sql_custId);
			$customerId=$res_custId['id_customer'];
			$custlocation=$res_custId['location'];
			
			$sql_cartId=mysql_query("SELECT id_cart FROM `ps_cart` where id_customer=$customerId");
			$res_cartId=mysql_fetch_assoc($sql_cartId);
			$cartId=$res_cartId['id_cart'];
			
			/*$sql_currency=mysql_query("SELECT id_currency FROM `ps_currency` where iso_code='BHR'");
			$res=mysql_fetch_assoc($sql_currency);
			$currency=$res['id_currency'];*/
			
      $sql_shop=mysql_query("SELECT id_shop FROM `ps_zone_shop` where id_zone IN (SELECT id_zone FROM `ps_zone` where name='$custlocation') LIMIt 1");
			$res=mysql_fetch_assoc($sql_shop);
			$shopId=$res['id_shop'];
			
      $sql_shopgroup=mysql_query("SELECT id_shop_group FROM `ps_shop` where id_shop=$shopId ");
			$res=mysql_fetch_assoc($sql_shopgroup);
			$shopGroup=$res['id_shop_group'];

			$cartTotal=cart_sum($cartId);
			
      $sql=mysql_query("SELECT value FROM `ps_configuration` where name = 'ps_shipping_free_price'");//gives shipping charges as entered in shipping preferences
      $row=mysql_fetch_assoc($sql);
      $minCharges=$row['value'];
      
      $sql=mysql_query("SELECT value FROM `ps_configuration` where name = 'ps_shipping_handling'");//gives shipping charges if cartTotal is <3 BD
      $row=mysql_fetch_assoc($sql);
      $shippingCharge=$row['value'];
      
      if($cartTotal>$minCharges){
          $shippingCharges=0.000;
      }
      else{
          $shippingCharges=$shippingCharge;
      }

      if(!$shopId)
      {
        $result=array('result'=> array('status' => 1,'message' =>$msgcode[133],'version'=>"1.0",'msgcode'=>133));
        echo json_encode($result); 
        exit(0);
      }

      //payment and module are mandatory to change order status in back-office -akshata
      //inserting timeslot into delivery_number column and inserting delivery date in orders table - Dhanunjaya
			$sql_placeorder=mysql_query("INSERT INTO `ps_orders`(id_shop_group,id_shop,id_customer,id_cart,id_address_delivery,total_paid,date_add,total_shipping,current_state,delivery_number,delivery_date) values ($shopGroup,$shopId,$customerId,$cartId,$addressId,'$cartTotal',NOW(),$shippingCharges,1,$timeslot,'$selectedDate')");
			if(!$sql_placeorder)
				die("invalid query place order    ".mysql_error());
			$sql_orderId=mysql_query("SELECT id_order FROM `ps_orders` where id_customer=$customerId order by id_order DESC ");
			$row_orderId=mysql_fetch_assoc($sql_orderId);
			$orderId=$row_orderId['id_order'];
			
      $sql_cart=mysql_query("SELECT * FROM `ps_cart_product` where id_cart=$cartId");
      $num_prod=mysql_num_rows($sql_cart);
      if($num_prod==0)
      {
          $result=array('result'=> array('status' => 0,'message' =>$msgcode[123],'version'=>"1.0",'msgcode'=>123));
          echo json_encode($result); 
          exit(0);
      }
      $sql_fromcart=mysql_query("SELECT id_product, id_product_attribute,quantity FROM `ps_cart_product` where id_cart=$cartId");
		
 
      while($row=mysql_fetch_assoc($sql_fromcart))
			{
				//print_r("inside delete from cart" );
				$productId=$row['id_product'];
				$attributeId=$row['id_product_attribute'];
				$quantity=$row['quantity'];
				$sql_prodname=mysql_query("SELECT name,description FROM ps_product_lang WHERE id_shop=1 and id_lang=1 and id_product=$productId");
    		$row_prodname=mysql_fetch_assoc($sql_prodname);
    		$name=$row_prodname['name'];
    		//print_r("name : ".$name);
    		if($attributeId==0)
    		{
    			//print_r("inside attribute id : not 0 ");
    			$sql=mysql_query("SELECT price FROM ps_product where id_product=$productId");
         	$row=mysql_fetch_assoc($sql);
         	$actualPrice=$row['price'];
         	//print_r("actual: ".$actualPrice);
         	$sql=mysql_query("SELECT reduction FROM ps_specific_price where id_product=$productId");
         	$row=mysql_fetch_assoc($sql);
         	$discountAmount=$row['reduction'];

         	$sql=mysql_query("SELECT id_feature_value FROM `ps_feature_product` where id_feature=4 and id_product=$productId");
         	$row=mysql_fetch_assoc($sql);
         	$id_feature=$row['id_feature_value'];
         	$sql=mysql_query('SELECT value FROM `ps_feature_value_lang` where id_feature_value='.$id_feature);
         	$row=mysql_fetch_assoc($sql);
         	$measure=$row['value'];
         	//print_r($measure);

        }
    		else
    		{
    			$sql=mysql_query("SELECT id_attribute FROM ps_product_attribute_combination where id_product_attribute=$attributeId");
             	while($row2=mysql_fetch_assoc($sql))
               {
                  $id_attribute=$row2['id_attribute'];
                 
                  $sql1=mysql_query("SELECT id_attribute_group FROM ps_attribute where id_attribute=$id_attribute");
                  $row=mysql_fetch_assoc($sql1);
                  $id_attribute_group=$row['id_attribute_group'];
                  $sql2=mysql_query("SELECT name FROM ps_attribute_group_lang where id_attribute_group=$id_attribute_group");
                  $row=mysql_fetch_assoc($sql2);
                  $attri_name=$row['name'];
                  $sql3=mysql_query("SELECT name FROM `ps_attribute_lang` where id_attribute=$id_attribute");
                  $row=mysql_fetch_assoc($sql3);
                  $variant[$attri_name]=$row['name'];
                }
              $actualPrice=$variant['actualPrice']; 
              $measure=$variant['measure'];
              if(!$variant['discountPrice'])
                  $discountAmount='';
              else 
                  $discountAmount=$actualPrice-$variant['discountPrice'];
              $variant=[];
        }
        $sql_insertorder=mysql_query("INSERT INTO `ps_order_detail`(id_order,id_shop,product_id,product_attribute_id,product_name,product_quantity,product_price,reduction_amount,product_weight) values ($orderId,$shopId,$productId,$attributeId,'$name',$quantity,$actualPrice,'$discountAmount','$measure')");
        if(!$sql_insertorder)
          die("invalid query insert order detail ".mysql_error());
        $sql_deletecart=mysql_query("DELETE FROM `ps_cart_product` WHERE `id_cart` =$cartId");
        $current_date = date("Y-m-d H:i:s");
        $sql_orderhistory=mysql_query("INSERT INTO `ps_order_history` (id_order,id_order_state,date_add) values ($orderId,1,'$current_date')");
        if(!$sql_orderhistory)
          die("invalid insert to order history ".mysql_error());
      }
      
		
	     //print_r($selectedDate);
      $sql_slot_available = mysql_query("SELECT * from tc_shop_delivery_slots_availability WHERE shop_id = $shopId AND order_date = '$selectedDate' AND slot_id = $timeslot");          
      $slot_available=mysql_num_rows($sql_slot_available);
      
      if ($slot_available != 0) {
            $sql_slot_available_row=mysql_fetch_assoc($sql_slot_available);
            $order_nb = $sql_slot_available_row['nb_order'];
            $sql_update_slot=mysql_query("UPDATE `tc_shop_delivery_slots_availability` set nb_order=$order_nb+1 where slot_id=$timeslot");
      }
      else{
        
            $sql_insert_slot=mysql_query("INSERT INTO `tc_shop_delivery_slots_availability` (shop_id,order_date,slot_id,nb_order) values ($shopId,'".$selectedDate."',$timeslot,1)");
            //echo "INSERT INTO `tc_shop_delivery_slots_availability` (shop_id,order_date,slot_id,nb_order) values ($shopId,'".$selectedDate."',$timeslot,1)";
          
      }  
    }
	}
  $sql=mysql_query("SELECT start,end FROM `tc_shop_delivery_slots` where id=$timeslot");
  $row=mysql_fetch_assoc($sql);
  $start=$row['start'];
  $end=$row['end'];
  //Changes the date format for Orer date and add the parameter delivery date
	$result=array('result'=> array('status' => 1,'message' =>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'orderId'=>$orderId,'date'=>date("d M Y h:i A"),'time'=>$start." - ".$end,'delivery_date'=>date("d M Y", strtotime($selectedDate)));
    echo json_encode($result); 
}
?>
