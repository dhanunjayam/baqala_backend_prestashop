<?php 
include("dbdata.php");
include("msgcode.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
//header('Content-Type: application/form-data');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);
$data = json_decode(file_get_contents('php://input'), true);
$guestproducts=$data["guestproducts"];
//print_r($guestproducts);
if(!$conn)
{
  $result=array('result'=>array('status'=>2,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  echo json_encode($result);
}
else
{
  $cart=array();
  foreach($guestproducts as $value)
      {
            $product['id']=$value[id];
            $product['variantId']=$value[variantId];
            $product['selectedQty']=$value[selectedQty];


        if($value[variantId]==0)
        {
            $sql_pName=mysql_query('SELECT name FROM ps_product_lang WHERE id_product='.$product['id']);
            $row=mysql_fetch_assoc($sql_pName);
            $product['name']=$row['name'];

            $sql_actPrice=mysql_query('SELECT price FROM ps_product where id_product='.$product['id']);
            $row=mysql_fetch_assoc($sql_actPrice);
            $product['actualPrice']=$row['price'];
                  //print_r("actual price ".$product['actualPrice']);
                  //print_r("actual price ".$row['price']);

            $sql_disPrice=mysql_query('SELECT reduction FROM ps_specific_price where id_product='.$product['id']);
            $row=mysql_fetch_assoc($sql_disPrice);
            $discountAmount=$row['reduction'];
            if($discountAmount>0)
                {
                    $product['discountPrice']=$product['actualPrice']-$discountAmount;
                    $cartTotal=$cartTotal+($product['discountPrice']*$product['selectedQty']);
                    $product['discountPrice']="BD ".number_format(($product['actualPrice']-$discountAmount),3);
                }
            else
                {
                    $cartTotal=$cartTotal+($product['actualPrice']*$product['selectedQty']);
                    $product['discountPrice']="";
                }
            $product['actualPrice']="BD ".number_format($product['actualPrice'],3);
            $sql_measure=mysql_query('SELECT id_feature_value FROM `ps_feature_product` where id_feature=4 and id_product='.$product['id']);
            $row=mysql_fetch_assoc($sql_measure);
            $id_feature=$row['id_feature_value'];
            $sql_measure=mysql_query('SELECT value FROM `ps_feature_value_lang` where id_feature_value='.$id_feature);
            $row=mysql_fetch_assoc($sql_measure);
            $product['measure']=$row['value']; 
            $sql = mysql_query('SELECT id_image FROM ps_image where id_product='.$product['id'].' and cover=1'); 
            $num_rows=mysql_num_rows($sql);
            if($num_rows==0) {
                 $product['image']="http://52.33.163.150/img/p/en.jpg";
                 } 
            else
                {    
                 $product['image']="http://52.33.163.150/img/p/";          
                 while($row=mysql_fetch_assoc($sql)) 
                     {
                      $arr = str_split($row['id_image'],1);
                      foreach($arr as $a)
                          {
                           $product['image']=$product['image'].$a.'/';
                           }
                      $product['image']=$product['image'].$row['id_image'].'.jpg';
                     }
                }// fetched product details without any varience
                array_push($cart,$product);//print_r($products);
                $product=[];
        }
        else
        {
            $sql_pName=mysql_query('SELECT name FROM ps_product_lang WHERE id_product='.$product['id']);
            $row=mysql_fetch_assoc($sql_pName);
            $product['name']=$row['name'];
            $variant=$product['variantId'];
            $sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant.' ORDER BY id_image DESC LIMIT 1');
            //$sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant);
            $num_rows=mysql_num_rows($sql);
            if($num_rows==0) 
                {
                $product['image']="http://52.33.163.150/img/p/en.jpg";
                } 
            else{  
                $product['image']="http://52.33.163.150/img/p/";
                while($row=mysql_fetch_assoc($sql))
                    {
                    $arr = str_split($row['id_image'],1);
                    foreach($arr as $a)
                       {
                       $product['image']=$product['image'].$a.'/';
                        }
                    $product['image']=$product['image'].$row['id_image'].'.jpg';
                    }
                } 
                $sql='SELECT id_attribute FROM ps_product_attribute_combination where id_product_attribute='.$variant;
                $res_attributes=mysql_query($sql);
                while($row2=mysql_fetch_assoc($res_attributes))
                      {
                      $id_attribute=$row2['id_attribute'];
                      $sql='SELECT id_attribute_group FROM ps_attribute where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $id_attribute_group=$row['id_attribute_group'];
                      $sql='SELECT name FROM ps_attribute_group_lang where id_attribute_group='.$id_attribute_group;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $attri_name=$row['name'];
                      $sql='SELECT name FROM `ps_attribute_lang` where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $product[$attri_name]=$row['name'];
                      }
                if(!$product['discountPrice']){
                      $product['discountPrice']="";
                      $cartTotal=$cartTotal+($product['actualPrice']*$product['selectedQty']);
                      }
                else 
                    {
                    $cartTotal=$cartTotal+($product['discountPrice']*$product['selectedQty']);
                    $product['discountPrice']="BD ".number_format($product['discountPrice'],3);
                    }           
                $product['actualPrice']="BD ".number_format($product['actualPrice'],3);
                array_push($cart,$product);
                $product=[];
        }
	   }
  	$sql=mysql_query("SELECT value FROM `ps_configuration` where name = 'ps_shipping_free_price'");//gives shipping charges as entered in shipping preferences
    $row=mysql_fetch_assoc($sql);
    $minCharges=$row['value'];
      
    $sql=mysql_query("SELECT value FROM `ps_configuration` where name = 'ps_shipping_handling'");//id_cart_rule=1 for minimum amount cart amount=3
    $row=mysql_fetch_assoc($sql);
    $shippingCharge=$row['value'];
    /*if($cartTotal>$minCharges)
      {
      $shippingCharge='BD 0.000';
      $amtForFreeShipping="";
      }
    else
      {
      $shippingCharge="BD 0.5";
      $amtForFreeShipping=$minCharges-$cartTotal;
      $amtForFreeShipping="BD ".$amtForFreeShipping;
      }*/

      $shippingCharge="BD ".number_format($shippingCharge,3);
      $otherCharges="";
      $minCharges="BD ".number_format($minCharges,3);
      $cartTotal="BD ".number_format($cartTotal,3);
    $cartarray=array('products'=>$cart,'cartId'=>"0101010",'cartTotal'=>strval($cartTotal), 'shippingCharge'=>$shippingCharge,'otherCharges'=>$otherCharges,'amtForFreeShipping'=>$minCharges);
    $result=array('result'=> array('status' => 1,'message' =>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'cart'=>$cartarray);
    echo json_encode($result); 
}
?>