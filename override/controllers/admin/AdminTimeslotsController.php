
<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Country $object
 */

class AdminTimeslotsControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'shop';
        $this->className = 'Shop';
        $this->lang = false;
        $this->deleted = false;
        $this->_defaultOrderBy = 'name';
        $this->_defaultOrderWay = 'ASC';

        $this->explicitSelect = false;
        $this->addRowAction('edit');

        $this->context = Context::getContext();


        $this->fields_list = array(
            'id_shop' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'name' => array(
                'title' => $this->l('shop'),
                'filter_key' => 'name'
            ),
        );

        parent::__construct();
    }

    public function initPageHeaderToolbar()
    {
        if (empty($this->display)) {
            // $this->page_header_toolbar_btn['new_country'] = array(
            //     'href' => self::$currentIndex.'&addcountry&token='.$this->token,
            //     'desc' => $this->l('Add new country', null, null, false),
            //     'icon' => 'process-icon-new'
            // );
        }

        parent::initPageHeaderToolbar();
    }

    /**
     * AdminController::setMedia() override
     * @see AdminController::setMedia()
     */
    public function setMedia()
    {
        parent::setMedia();

        $this->addJqueryPlugin('fieldselection');
    }

    public function renderList()
    {
        $this->_select = 'name';
        return parent::renderList();
    }

    public function renderForm()
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        // $address_layout = AddressFormat::getAddressCountryFormat($obj->id);
        // if ($value = Tools::getValue('address_layout')) {
        //     $address_layout = $value;
        // }

         $shop_id = (int)Tools::getValue('id_shop');
         //echo $shop_id;

        $default_layout = '';

        $default_layout_tab = array(
            array('firstname', 'lastname'),
            array('company'),
            array('vat_number'),
            array('address1'),
            array('address2'),
            array('postcode', 'city'),
            array('Shop:name'),
            array('phone'),
            array('phone_mobile'));

        foreach ($default_layout_tab as $line) {
            $default_layout .= implode(' ', $line)."\r\n";
        }

       

         $fetch_array=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS("SELECT * FROM `tc_shop_delivery_slots` where shop_id='$shop_id' and active=1 ORDER BY day ASC, start ASC");
        // echo "SELECT * FROM `tc_shop_delivery_slots` where shop_id='$shop_id'";
        // echo sizeof($fetch_array);
         $slots_array = array();
        if (sizeof($fetch_array)) {
            $counter = 0;
            $old_day = null;
            foreach ($fetch_array as $row) { 
                $tempArray = array(
                        'slot_id' => $row['id'],
                        'shop_id' => $row['shop_id'],
                        'day'  => $row['day'],
                        'start' => $row['start'],
                        'end' => $row['end'],
                        'limit' => $row['max_limit'], 
                    );            
                $day = $row['day'];
                if ($day != $old_day) {
                    $counter = 0;
                }
                $old_day = $day;
                
                $slots_array[$day][$counter] = $tempArray;
                $counter++;
            }
            
            for ($i=1; $i < 8; $i++) { 
                if(!array_key_exists($i, $slots_array)) {
                    $slots_array[$i][0]=array(
                                        'slot_id' => '',
                                        'shop_id' => '',
                                        'day'  => '',
                                        'start' => '',
                                        'end' => '',
                                        'limit' => '', 
                                        );
                }
            }
            ksort($slots_array);
        }
        else{
            for ($i=1; $i < 8; $i++) { 
                $slots_array[$i][0]=array(
                                        'slot_id' => '',
                                        'shop_id' => '',
                                        'day'  => '',
                                        'start' => '',
                                        'end' => '',
                                        'limit' => '', 
                                        );
            }   
            // echo "<pre>";
            // print_r($slots_array);
            // die();
         }
        
        $this->context->smarty->assign('slotsArray',$slots_array);
        
        //End of timeslots

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Timeslots'),
                'icon' => 'icon-globe'
            ),
            
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Shop'),
                    'name' => 'name',
                    'hint' => $this->l('Shop name').' - '.$this->l('Invalid characters:').' &lt;&gt;;=#{} '
                ),
                 array(
                    'type' => 'timeslots_layout',
                    'label' => $this->l('Time Slots'),
                    'name' => 'timeslots_layout',
                    'hint' => $this->l('Time Slots').' - '.$this->l('Invalid characters:').' &lt;&gt;;=#{} '
                ),
            )

        );

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save')
        );

        return parent::renderForm();
    }

     public static function getDayFromNumber($dayNo)
    {
        $dayStr = "";

        switch ($dayNo) {
            case '1':
                $dayStr = "Monday";
                break;
            case '2':
                $dayStr = "Tuesday";
                break;
            case '3':
                $dayStr = "Wednesday";
                break;
            case '4':
                $dayStr = "Thursday";
                break;
            case '5':
                $dayStr = "Friday";
                break;
            case '6':
                $dayStr = "Saturday";
                break;
            case '7':
                $dayStr = "Sunday";
                break;
            
            default:
                $dayStr = "";
                break;
        }

        return $dayStr;
    }

    public function processUpdate()
    {
        /** @var Country $country */
        $country = $this->loadObject();
        if (Validate::isLoadedObject($country) && Tools::getValue('id_zone')) {
            $old_id_zone = $country->id_zone;
            $results = Db::getInstance()->executeS('SELECT `id_state` FROM `'._DB_PREFIX_.'state` WHERE `id_country` = '.(int)$country->id.' AND `id_zone` = '.(int)$old_id_zone);

            if ($results && count($results)) {
                $ids = array();
                foreach ($results as $res) {
                    $ids[] = (int)$res['id_state'];
                }

                if (count($ids)) {
                    $res = Db::getInstance()->execute(
                            'UPDATE `'._DB_PREFIX_.'state`
							SET `id_zone` = '.(int)Tools::getValue('id_zone').'
							WHERE `id_state` IN ('.implode(',', $ids).')');
                }
            }
        }
        return parent::processUpdate();
    }

    public function postProcess()
    {

        // if (!Tools::getValue('id_'.$this->table)) {
        //     if (Validate::isLanguageIsoCode(Tools::getValue('iso_code')) && (int)Country::getByIso(Tools::getValue('iso_code'))) {
        //         $this->errors[] = Tools::displayError('This ISO code already exists.You cannot create two countries with the same ISO code.');
        //     }
        // } elseif (Validate::isLanguageIsoCode(Tools::getValue('iso_code'))) {
        //     $id_country = (int)Country::getByIso(Tools::getValue('iso_code'));
        //     if (!is_null($id_country) && $id_country != Tools::getValue('id_'.$this->table)) {
        //         $this->errors[] = Tools::displayError('This ISO code already exists.You cannot create two countries with the same ISO code.');
        //     }
        // }

        return parent::postProcess();
    }

    public function processSave()
    {

        $timeslots_array = Tools::getValue('Timeslots');


	//print_r($timeslots_array);
	//die();

        $shop_id = (int)Tools::getValue('id_shop');

        foreach ($timeslots_array as $day_key => $day_value) {
            foreach ($day_value as $slot_key => $slot_value) {
                //print_r("slot->".$slot_value['end']."\r\n");
                $slot_id = $slot_value['slotid'];

                $slot_id_length = strlen($slot_id);
                $start_var = $slot_value['start'];
                $end_var = $slot_value['end'];
                $limt_var = $slot_value['limit'];
                $deleted_var = $slot_value['deleted'];

                if($deleted_var == 1 && $slot_id_length == 0) {
                   //Do Nothing
                } else if ($deleted_var == 1 && $slot_id_length != 0) {
                   Db::getInstance()->execute("UPDATE `tc_shop_delivery_slots` SET active = '0' WHERE id = $slot_id");
                } else if ($deleted_var != 1) {
                    if($slot_id_length == 0) {
                        Db::getInstance()->execute("INSERT INTO `tc_shop_delivery_slots` (`shop_id`, `day`, `start`, `end`, `max_limit`) VALUES ($shop_id,$day_key,'$start_var','$end_var',$limt_var)");
                    } else {
                        Db::getInstance()->execute("UPDATE `tc_shop_delivery_slots` SET start = '$start_var', end = '$end_var', max_limit = $limt_var WHERE id = $slot_id");
                    }
                }
            }
        }
        
        $country = parent::processSave();

        return $country;
    }

    public function processStatus()
    {
        parent::processStatus();

        // /** @var Country $object */
        // if (Validate::isLoadedObject($object = $this->loadObject()) && $object->active == 1) {
        //     return Country::addModuleRestrictions(array(), array(array('id_country' => $object->id)), array());
        // }

        return false;
    }

    public function processBulkStatusSelection($way)
    {
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $countries_ids = array();
            foreach ($this->boxes as $id) {
                $countries_ids[] = array('id_country' => $id);
            }

            if (count($countries_ids)) {
                Country::addModuleRestrictions(array(), $countries_ids, array());
            }
        }
        parent::processBulkStatusSelection($way);
    }
}

