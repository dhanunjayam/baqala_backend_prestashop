{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{extends file="helpers/form/form.tpl"}
	
{block name="field"}
	<!-- {if $input.type == 'address_layout'}
		<div class="col-lg-9">
			<div class="form-group">
				<div class="col-lg-4">
					<textarea id="ordered_fields" name="address_layout" style="height:150px;">{$input.address_layout}</textarea>
				</div>
				<div class="col-lg-8">
					{l s='Required fields for the address (click for more details):'}
					{$input.display_valid_fields}
				</div>
			</div>			
			<div class="row">
				<div class="col-lg-12">
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='This will restore your last registered address format.'}" data-html="true"><a id="useLastDefaultLayout" href="javascript:void(0)" onclick="resetLayout('{$input.encoding_address_layout}', 'lastDefault');" class="btn btn-default">
						{l s='Use the last registered format'}</a></span>
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='This will restore the default address format for this country.'}" data-html="true"><a id="useDefaultLayoutSystem" href="javascript:void(0)" onclick="resetLayout('{$input.encoding_default_layout}', 'defaultSystem');" class="btn btn-default">
						{l s='Use the default format'}</a></span>
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='This will restore your current address format.'}" data-html="true"><a id="useCurrentLastModifiedLayout" href="javascript:void(0)" onclick="resetLayout(lastLayoutModified, 'currentModified')" class="btn btn-default">
						{l s='Use my current modified format'}</a></span>
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='This will delete the current address format'}" data-html="true"><a id="eraseCurrentLayout" href="javascript:void(0)" onclick="resetLayout('', 'erase');" class="btn btn-default">
						<i class="icon-eraser"></i> {l s='Clear format'}</a></span>
				</div>
			</div>
		</div>
	{else}
		{$smarty.block.parent}
	{/if} -->
	<!-- {if $input.name == 'shop_layout'}
	<div class="col-lg-9">
			<div class="form-group">
				<div class="col-lg-4">
					<input name="shop_name" id="shop_name" type="text" />
				</div>
			</div>	
	</div >
	{else}
		{$smarty.block.parent}
	{/if} -->
	{if $input.name == 'timeslots_layout'}
	    <div class="col-lg-9">
			<div class="panel">
				{assign var=dayCounter value=0}
					{foreach $slotsArray as $key => $item}
						{assign var=dayCounter value=$dayCounter+1}
						<div class="slotDay" data-dayid="{$dayCounter}">
					    	<div class="control-label col-lg-3" style="width:200px;text-align: left;margin-bottom:10px;">
					    		<label for="day_str">{AdminTimeslotsControllerCore::getDayFromNumber($key)}</label>
					    		<a style="float:right;" href="javascript:void(0)" alt="Add Time Slot" class="addRow">
					    			<img style="width:20px;" src="/img/admin/slot-plus.png" alt="Add Time Slot" />
					    		</a>
							</div>
							{assign var=slotCounter value=0}
							{assign var=counter value=0}
							{foreach $item as $subItemKey => $subItemValue}	
								{assign var=counter value=$counter+1}					     
								<div class="form-group" style="width:270px;">
									<div class="col-lg-3" >
										<input name="Timeslots[{$dayCounter}][{$slotCounter}][slotid]" type="hidden" value="{$subItemValue['slot_id']}"/>
										<input name="Timeslots[{$dayCounter}][{$slotCounter}][start]" type="text" placeholder="hh:mm" class="timeClass" value="{$subItemValue['start']}"/>
									</div>
									<div class="col-lg-3" >
										<input name="Timeslots[{$dayCounter}][{$slotCounter}][end]" type="text" placeholder="hh:mm" class="timeClass" value="{$subItemValue['end']}"/>
									</div>
									<div class="col-lg-3">
										<input name="Timeslots[{$dayCounter}][{$slotCounter}][limit]" type="text" placeholder="Limit" value="{$subItemValue['limit']}"/>
									</div>
									<div class="col-lg-3">
										<input name="Timeslots[{$dayCounter}][{$slotCounter}][deleted]" type="hidden" class="deleted" value="{$subItemValue['deleted']}"/>
										<a href="javascript:void(0)" alt="Remove Time Slot" class="deleteRow"><img src="/img/admin/slot-minus.png" style="width:20px;margin-top:5px;" alt="Remove Time Slot"></a>
									</div>
								</div>
								{assign var=slotCounter value=$slotCounter+1}
							{/foreach}	
							
						</div>
					{/foreach}
					
				</div>					
			</div>
		</div>

	{else}
		{$smarty.block.parent}
	{/if}
	
{/block}

{block name="input_row"}
	{if $input.name == 'standardization'}
		<div class="form-group" id="TAASC" style="display: none;">
			<label for="{$input.name}" class="control-label col-lg-3">{$input.label}</label>
			<div class="col-lg-9">
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="{$input.name}" id="{$input.name}_on" value="1" />
					<label for="{$input.name}_on">
						{l s='Yes'}
					</label>
					<input type="radio" name="{$input.name}" id="{$input.name}_off" value="0" checked="checked" />
					<label for="{$input.name}_off">
						{l s='No'}
					</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}


{block name=script}

	$(document).ready(function() {

		$('.addPattern').click(function() {
			addFieldsToCursorPosition($(this).attr("id"))
			lastLayoutModified = $("#ordered_fields").val();
		});

		$('#ordered_fields').keyup(function() {
			lastLayoutModified = $(this).val();
		});

		$('#need_zip_code_on, #need_zip_code_off').change(function() {
			disableZipFormat();
		});
		
		$('#iso_code').change(function() {
			disableTAASC();
		});
		disableTAASC();

		<!-- var addSlotRowTemplate = '<div class="form-group" style="width:270px;"><div class="col-lg-3" ><input name="Timeslots[DAYID][SLOTID][slotid]" type="hidden" value=""/><input name="Timeslots[DAYID][SLOTID][start]" type="text" placeholder="hh:mm" class="timeClass" value=""/></div><div class="col-lg-3" ><input name="Timeslots[DAYID][SLOTID][end]" type="text" placeholder="hh:mm" class="timeClass" value=""/></div><div class="col-lg-3"><input name="Timeslots[DAYID][SLOTID][limit]" type="text" placeholder="Limit" value=""/></div><div class="col-lg-3"><input name="Timeslots[DAYID][SLOTID][deleted]" class="deleted" type="hidden" value=""/><a href="javascript:void(0)" alt="Remove Time Slot" class="deleteRow"><img src="/img/admin/slot-minus.png" style="width:20px;margin-top:5px;" alt="Remove Time Slot"></a></div></div>';
		 -->
		$('.addRow').click(function() {

		var addSlotRowTemplate = '<div class="form-group" style="width:270px;"><div class="col-lg-3" ><input name="Timeslots[DAYID][SLOTID][slotid]" type="hidden" value=""/><input name="Timeslots[DAYID][SLOTID][start]" type="text" placeholder="hh:mm" class="timeClass" value=""/></div><div class="col-lg-3" ><input name="Timeslots[DAYID][SLOTID][end]" type="text" placeholder="hh:mm" class="timeClass" value=""/></div><div class="col-lg-3"><input name="Timeslots[DAYID][SLOTID][limit]" type="text" placeholder="Limit" value=""/></div><div class="col-lg-3"><input name="Timeslots[DAYID][SLOTID][deleted]" class="deleted" type="hidden" value=""/><a href="javascript:void(0)" alt="Remove Time Slot" class="deleteRow"><img src="/img/admin/slot-minus.png" style="width:20px;margin-top:5px;" alt="Remove Time Slot"></a></div></div>';

			var thisElement = $(this);
			var parentElement = thisElement.parents(".slotDay");
			
			var availableRowLength = parentElement.find('.form-group').length;
			//Modify DayId and SlotId
			var dayNo = parentElement.data('dayid');
			var slotNo = availableRowLength;
			console.log(dayNo+ " " +slotNo);
			addSlotRowTemplate = addSlotRowTemplate.replace(/DAYID/g, dayNo).replace(/SLOTID/g, slotNo);
			//alert(addSlotRowTemplate);
			parentElement.append(jQuery.parseHTML(addSlotRowTemplate));
		});

		$('body').on('click', '.deleteRow', function () {
			var thisElement = $(this);
			
			thisElement.closest(".form-group").find(".deleted").val("1");
			thisElement.closest(".form-group").hide();
		});

		$('body').on('blur', '.timeClass', function () {
			var value = $(this).val();
			if (!value.match(/(2[0-3]|[01][0-9]):[0-5][0-9]/)) {
				$(this).addClass('error');
			} else {
				$(this).removeClass('error');
			}
		});
	});

	function addFieldsToCursorPosition(pattern) {
		$("#ordered_fields").replaceSelection(pattern + " ");
	}

	function resetLayout(defaultLayout, type) {
		if (confirm("{l s='Are you sure you want to restore the default address format for this country?' js=1}"))
		$("#ordered_fields").val(unescape(defaultLayout.replace(/\+/g, " ")));
	}
	
	$('#custom-address-fields a').click(function (e) {
  		e.preventDefault();
  		$(this).tab('show')
	})

{/block}
