<?php 
include("dbdata.php");
include("msgcode.php");
//error_reporting(E_ERROR);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);

if(!$conn)
{
  $result=array('result'=>array('status'=>2,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  echo json_encode($result);
}
 //select all products for a particular category
else {

   $product_sql='SELECT id_product, id_category_default, price, active  FROM ps_product';
   $product_query=mysql_query($product_sql);
   echo json_encode($product_query);
   //$attribute =array();
   $num_rows_in_product=mysql_num_rows($product_query);
    if($num_rows_in_product==0) {
          $result=array('result'=>array('status'=>0,'message'=>"No Products",'version'=>"1.0",'msgcode'=>109));
          echo json_encode($result);
    }
    else 
    {
        $products = array();
        while($product_row = mysql_fetch_assoc($product_query)) {

        	$product['product_id'] = $product_row['id_product'];
        	$product['status'] = $product_row['active'];
        	$product['actual_price'] = "BD ".number_format($product_row['price'],3);

        	//Fetching Category name and sub category name
        	$subcategory_id = $product_row['id_category_default']; // taking id_category_default as subcategory_id from ps_product table

        	$cateogry_sql='SELECT id_parent FROM ps_category WHERE id_shop_default=1 and id_category='.$subcategory_id;
         	$category_query=mysql_query($cateogry_sql);
         	$category_row=mysql_fetch_assoc($category_query);
         	$category_id = $category_row['id_parent'];

			$cateogry_lang_sql='SELECT name FROM ps_category_lang WHERE id_shop=1 and id_lang=1 and id_category='.$category_id;
         	$category_lang_query=mysql_query($cateogry_lang_sql);
         	$category_lang_row=mysql_fetch_assoc($category_lang_query);
         	$product['category_name'] = $category_lang_row['name'];


        	$subcateogry_lang_sql='SELECT name FROM ps_category_lang WHERE id_shop=1 and id_lang=1 and id_category='.$subcategory_id;
         	$subcategory_lang_query=mysql_query($subcateogry_lang_sql);
         	$subcategory_lang_row=mysql_fetch_assoc($subcategory_lang_query);
         	$product['subcategory_name'] = $subcategory_lang_row['name'];

        	//Fetching name from ps_product_lang with id_product
        	$product_lang_sql='SELECT name FROM ps_product_lang WHERE id_shop=1 and id_lang=1 and id_product='.$product['product_id'];
         	$product_lang_query=mysql_query($product_lang_sql);
         	$product_lang_row=mysql_fetch_assoc($product_lang_query);
         	$product['name']=ucwords(strtolower($product_lang_row['name']));
         	$product['og_title']=ucwords(strtolower($product_lang_row['name']));

         	//Fetching reduction from ps_specific_price using id_product
            $specific_price_sql=mysql_query('SELECT reduction FROM ps_specific_price where id_product='.$product['product_id']);
            $specific_price_row=mysql_fetch_assoc($specific_price_sql);
            $discountPrice = $specific_price_row['reduction'];

            if($discountPrice == ""){
				$product['discount_price']="BD 0.00";
            }
            else{
		       $product['discount_price']="BD ".number_format($discountPrice,3);
            }


            //Fetching Image from ps_image table using id_product
            $image_sql = mysql_query('SELECT id_image FROM ps_image where id_product='.$product['product_id'].' and cover=1'); 
            $image_num_rows=mysql_num_rows($image_sql);
            if($image_num_rows==0) {
                $product['og_image_url']=$hostURL."/img/p/en.jpg";
            } 
            else
            {    
              $product['image']=$hostURL."/img/p/";          
              while($image_row=mysql_fetch_assoc($image_sql)) 
              {
                 $arr = str_split($image_row['id_image'],1);
                 foreach($arr as $a)
                 {
                    $product['image']=$product['image'].$a.'/';
                 }
                 $product['og_image_url']=$product['image'].$image_row['id_image'].'.jpg';
               }
            }


         	//Adding product to products Arrray
     //     	$products_array = array(
  			//   'data' => json_encode($product),
  			//   "campaign"=> "campaignA",
  			//   "channel"=> "branch", 
  			//   "identity" => "bulk"
  			// );
			array_push($products,$product);

         	$product=[];
        }
        download_send_headers("baqala_product_feed" . date("Y-m-d") . ".csv");
		echo array2csv($products);
		die();

  //       //Preparing POST request and calling Branch bulk API
  //       //Branchkey - Production
  //       $results_array = array();
  //       $start = 0;
  //       $array_count = sizeof($products);
  //       print_r("array_count+++++".$array_count);
  //       $end = 100;
		// while($start < $array_count) { 
			

  //      		$output_array = array_slice($products, $start, $end, true);
  //     		$url = "https://api.branch.io/v1/url/bulk/key_live_cneO8JR3g0ieYOQeIKelgjklqxi7a3h4";
		// 	$url = str_replace(' ', '%20', $url);
		// 	$data = json_encode($output_array);
		// 	//print_r("output_array".$data);

		// // print_r(strlen($data));
		// 	// $size = strlen($data);
		// 	// $size = ($size * 8 / 1000);
			
		
		// 	print_r("start+++++".$start);
		// 	print_r("end+++++".$end);


		// 	$curl = curl_init($url);
  //   		curl_setopt($curl, CURLOPT_POST, true);
  //   		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  //   		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  //   		curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  //   		$response = curl_exec($curl);
  //   		curl_close($curl);

		// 	//$result = httpPost($url,$data);
		// 	array_push($results_array,$response);
		// 	$start = $end + 1;
		// 	$end += 100;
		// }        
    }
}

function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}
function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

function httpPost($url, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

?>