<?php 
include("dbdata.php");
include("msgcode.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
//header('Content-Type: application/form-data');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);

if(!$conn)
{
	$result=array('result'=>array('status'=>0,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  	echo json_encode($result);
}
else 
{
	if(!$_GET['accessToken']) 
	{
  		$result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  		echo json_encode($result);
  		exit(0);
	}
	else
	{
		$accessToken=$_GET['accessToken'];
		$sql_custId=mysql_query("SELECT id_customer from ps_customer where access_token = '$accessToken' ");
		$num=mysql_num_rows($sql_custId);
		if($num==0)
		{
			$result=array('result'=>array('status'=>0,'message'=>$msgcode[117],'version'=>"1.0",'msgcode'=>117));
  			echo json_encode($result);	
		}
		else
		{
			$res=mysql_fetch_assoc($sql_custId);
			$custId=$res['id_customer'];
			//print_r($custId);
			$sql_orders=mysql_query("SELECT id_order,total_paid,total_shipping,current_state,delivery_date,slot_id FROM `ps_orders` where id_customer=$custId order by id_order DESC");
			$orders = array();
			while($row=mysql_fetch_assoc($sql_orders))
			{
				$slot_id=$row['slot_id'];
				

				$sql_query_deliveryslots = mysql_query("SELECT start,end FROM tc_shop_delivery_slots where id = $slot_id");

	            $sql_query_deliveryslots_row=mysql_fetch_assoc($sql_query_deliveryslots);
	            $start_time=$sql_query_deliveryslots_row['start'];
	            $end_time=$sql_query_deliveryslots_row['end'];
	            $delivery_time_slot = $start_time."-".$end_time;

				$delivery_date = date("d M Y", strtotime($row['delivery_date']));
				$orderId=$row['id_order'];
				$order['orderId']=$orderId;
				$order['amount']=$row['total_paid']-$row['total_shipping'];
				$order['shippingCharges']=$row['total_shipping'];
				$orderstate=$row['current_state'];
								
				$sql=mysql_query("SELECT name FROM `ps_order_state_lang` where id_lang=1 and id_order_state=$orderstate");
				$row=mysql_fetch_assoc($sql);
				$order['status']=$row['name'];

				$sql_orderstateId=mysql_query("SELECT date_add FROM `ps_order_history` where id_order=$orderId order by date_add desc LIMIT 1");
				$row_orderstateId=mysql_fetch_assoc($sql_orderstateId);
				$order['orderDate']=$row_orderstateId['date_add'];
				$order=array(
								'orderId'=>$order['orderId'],
								'amount'=>"BD ".number_format($order['amount'],3),
								'orderDate'=>date("d M Y h:i A",strtotime($order['orderDate'])),
								'status'=>$order['status'],
								'delivery_date'=>$delivery_date,
								'delivery_time_slot'=>$delivery_time_slot,
								'shippingCharges'=>"BD ".number_format($order['shippingCharges'],3) );
				array_push($orders,$order);
				$order=[];
			}
			$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'customerId'=>$custId,'orderHistory'=>$orders);
			echo json_encode($result);
		}
	}
}
?>