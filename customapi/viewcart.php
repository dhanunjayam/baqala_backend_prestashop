<?php 
include("dbdata.php");
include("msgcode.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
//header('Content-Type: application/form-data');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);

if(!$_GET['accessToken']) {
  $result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  echo json_encode($result);
  exit(0);
}
else
{
	$access_token=$_GET['accessToken'];
	//print_r("access token : ".$access_token);
	$sql_custId=mysql_query("SELECT id_customer from ps_customer where access_token = '$access_token' ");
	$num=mysql_num_rows($sql_custId);
	if($num==0)
	{
		$result=array('result'=>array('status'=>0,'message'=>$msgcode[117],'version'=>"1.0",'msgcode'=>117));
  		echo json_encode($result);	
	}
	else
	{
		$res_custId=mysql_fetch_assoc($sql_custId);
		$customerId=$res_custId['id_customer'];
		//print_r("customer Id: ".$customerId);	
		$sql_cartId=mysql_query("SELECT id_cart FROM `ps_cart` where id_customer=$customerId ORDER BY id_cart DESC limit 1");
		$res_cartId=mysql_fetch_assoc($sql_cartId);
		$num=mysql_num_rows($sql_cartId);
		if($num==0)
		{
			$result=array('result'=>array('status'=>0,'message'=>$msgcode[118],'version'=>"1.0",'msgcode'=>118));
  			echo json_encode($result);	
		}
		else
		{
			$cart=array();
      $cartTotal=0.00;
			$cartId=$res_cartId['id_cart'];
			$sql_cartDetails=mysql_query("SELECT id_product_attribute,id_product,quantity FROM `ps_cart_product` where id_cart=$cartId");
			if(!sql_cartDetails)
				die("invalid query".mysql_error());
      $num=mysql_num_rows($sql_cartDetails);
      if($num == 0)
      {
          $result=array('result'=>array('status'=>0,'message'=>$msgcode[124],'version'=>"1.0",'msgcode'=>124));
          echo json_encode($result);  
      }
      else
      {
          while ($row=mysql_fetch_assoc($sql_cartDetails))
          {
            $product['id']=$row['id_product'];
            $variant=$row['id_product_attribute'];
            $product['variantId']=$variant;
            $product['selectedQty']=$row['quantity'];

            if($variant==0)
            {
                  $sql_pName=mysql_query('SELECT name FROM ps_product_lang WHERE id_product='.$product['id']);
                  $row=mysql_fetch_assoc($sql_pName);
                  $product['name']=$row['name'];

                  $sql_actPrice=mysql_query('SELECT price FROM ps_product where id_product='.$product['id']);
                  $row=mysql_fetch_assoc($sql_actPrice);
                  $product['actualPrice']=$row['price'];
                  //print_r("actual price ".$product['actualPrice']);
                  //print_r("actual price ".$row['price']);

                  $sql_disPrice=mysql_query('SELECT reduction FROM ps_specific_price where id_product='.$product['id']);
                  $row=mysql_fetch_assoc($sql_disPrice);
                  $discountAmount=$row['reduction'];
                  //echo "No Var ".$product['actualPrice']." ".$discountAmount;
                   //die;
                  if($discountAmount>0)
                  {
                    if ($discountAmount == $product['actualPrice']) {
                      $product['discountPrice']= "";
                       $cartTotal=$cartTotal+($product['actualPrice']*$product['selectedQty']);
                      
                    }
                    else{
                      //$product['discountPrice'] = $discountAmount;
                      $product['discountPrice']=$product['actualPrice']-$discountAmount;
                      $cartTotal=$cartTotal+($product['discountPrice']*$product['selectedQty']);
                      $product['discountPrice']="BD ".number_format(($product['actualPrice']-$discountAmount),3);
                    }
                  }else{
                      $cartTotal=$cartTotal+($product['actualPrice']*$product['selectedQty']);
                      $product['discountPrice']="";
                  }

                  $product['actualPrice']="BD ".number_format($product['actualPrice'],3);

                  $sql_measure=mysql_query('SELECT id_feature_value FROM `ps_feature_product` where id_feature=4 and id_product='.$product['id']);
                  $row=mysql_fetch_assoc($sql_measure);
                  $id_feature=$row['id_feature_value'];
                  $sql_measure=mysql_query('SELECT value FROM `ps_feature_value_lang` where id_feature_value='.$id_feature);
                  $row=mysql_fetch_assoc($sql_measure);
                  $product['measure']=$row['value']; 
                  $product['measure'] = $product['measure']==null?'':$product['measure'];

                  $sql = mysql_query('SELECT id_image FROM ps_image where id_product='.$product['id'].' and cover=1'); 
                  $num_rows=mysql_num_rows($sql);
                  if($num_rows==0) {
                        $product['image']=$hostURL."/img/p/en.jpg";
                      } 
                  else
                    {    
                        $product['image']=$hostURL."/img/p/";          
                        while($row=mysql_fetch_assoc($sql)) 
                      {
                           $arr = str_split($row['id_image'],1);
                            foreach($arr as $a)
                              {
                                $product['image']=$product['image'].$a.'/';
                              }
                            $product['image']=$product['image'].$row['id_image'].'.jpg';
                        }
                    }// fetched product details without any varience
                  array_push($cart,$product);//print_r($products);
                  $product=[];
            }
            else
            {
                  $sql_pName=mysql_query('SELECT name FROM ps_product_lang WHERE id_product='.$product['id']);
                  $row=mysql_fetch_assoc($sql_pName);
                  $product['name']=$row['name'];

                  $sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant.' ORDER BY id_image DESC LIMIT 1');
                    //$sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant);
                  $num_rows=mysql_num_rows($sql);
                  if($num_rows==0) 
                        {
                        $product['image']=$hostURL."/img/p/en.jpg";
                         } 
                  else{  
                        $product['image']=$hostURL."/img/p/";
                        while($row=mysql_fetch_assoc($sql))
                          {
                            $arr = str_split($row['id_image'],1);
                            foreach($arr as $a)
                            {
                              $product['image']=$product['image'].$a.'/';
                           }
                            $product['image']=$product['image'].$row['id_image'].'.jpg';
                          }
                        } 
                  $sql='SELECT id_attribute FROM ps_product_attribute_combination where id_product_attribute='.$variant;
                  $res_attributes=mysql_query($sql);
                  while($row2=mysql_fetch_assoc($res_attributes))
                  {
                    $id_attribute=$row2['id_attribute'];
                    $sql='SELECT id_attribute_group FROM ps_attribute where id_attribute='.$id_attribute;
                    $res=mysql_query($sql);
                    $row=mysql_fetch_assoc($res);
                    $id_attribute_group=$row['id_attribute_group'];
                    $sql='SELECT name FROM ps_attribute_group_lang where id_attribute_group='.$id_attribute_group;
                    $res=mysql_query($sql);
                    $row=mysql_fetch_assoc($res);
                    $attri_name=$row['name'];
                    $sql='SELECT name FROM `ps_attribute_lang` where id_attribute='.$id_attribute;
                    $res=mysql_query($sql);
                    $row=mysql_fetch_assoc($res);
                    $product[$attri_name]=$row['name'];
                    }
                    if(!$product['discountPrice']){
                      $product['discountPrice']="";
                      $cartTotal=$cartTotal+($product['actualPrice']*$product['selectedQty']);
                    }else{
                      if($product['discountPrice']==$product['actualPrice']){
                          $product['discountPrice']="";
                          $cartTotal=$cartTotal+($product['actualPrice']*$product['selectedQty']);
                          }
                        else{
                          $cartTotal=$cartTotal+($product['discountPrice']*$product['selectedQty']);
                          $product['discountPrice']="BD ".number_format($product['discountPrice'],3);
                        }                      
                      // $cartTotal=$cartTotal+($product['discountPrice']*$product['selectedQty']);
                      // $product['discountPrice']="BD ".number_format($product['discountPrice'],3);
                    }

                  $product['actualPrice']="BD ".number_format($product['actualPrice'],3);
                  array_push($cart,$product);
                  $product=[];
                 }
          }
      $sql=mysql_query("SELECT value FROM `ps_configuration` where name = 'ps_shipping_free_price'");//gives shipping charges as entered in shipping preferences
      $row=mysql_fetch_assoc($sql);
      $minCharges=$row['value'];
      
      $sql=mysql_query("SELECT value FROM `ps_configuration` where name = 'ps_shipping_handling'");//id_cart_rule=1 for minimum amount cart amount=3
      $row=mysql_fetch_assoc($sql);
      $shippingCharge=$row['value'];
      $shippingCharge="BD ".number_format($shippingCharge,3);
      $otherCharges="";
      $minCharges="BD ".number_format($minCharges,3);
      $cartTotal="BD ".number_format($cartTotal,3);
      $cartarray=array('products'=>$cart,'cartId'=>$cartId,'cartTotal'=>strval($cartTotal), 'shippingCharge'=>$shippingCharge,'otherCharges'=>$otherCharges,'amtForFreeShipping'=>$minCharges);
      $result=array('result'=> array('status' => 1,'message' =>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'cart'=>$cartarray);
      echo json_encode($result); 
	}
	     
		}
		
	}
}	
?>