<?php
$msgcode=array();
$msgcode['200']="success";
$msgcode['101']="Unable to connect to DB";
$msgcode['102']= "Invalid category Id";
$msgcode['103']="No sub-categories";
$msgcode['104']="No sub-categories for this category";
$msgcode['105']="locations unavailable";
$msgcode['106']="Delivery Unavailable in the preffered location";
$msgcode['107']="Invalid Parameter";
$msgcode['108']="No variant for this product";
$msgcode['109']="No products for this sub-category";
$msgcode['110']="Image unavaialble";
$msgcode['111']="Sorry! We could not find any product matching ";
$msgcode['112']="Registered user created";
$msgcode['113']="Guest user created";
$msgcode['114']="User already exists so OTP updated";
$msgcode['115']="Phone number is mandatory";
$msgcode['116']="Invalid OTP";
$msgcode['117']="Invalid access token";
$msgcode['118']="No cart associated with the customer";
$msgcode['119']="Cart updated";
$msgcode['120']="productId, variantId, selectedQty parameters are mandatory with access token";
$msgcode['121']="productId  and varaintId are manadatory to delete from cart";
$msgcode['122']="product deleted from cart";
$msgcode['123']="product doesnot exist in cart";
$msgcode['124']="There are no items in your cart. Start Shopping to enjoy great shopping experience with us!";
$msgcode['125']="Successful logout";
$msgcode['126']="accessToken,location,address1,address2 and addressType are mandatory";
$msgcode['127']="accessToken,location,address1,address2,addressType and addressId are mandatory";
$msgcode['128']="accessToken and addressId are mandatory";
$msgcode['129']="No addresses found";
$msgcode['130']="Phone number already exists";
$msgcode['131']="OTP generated for phone number updation";
$msgcode['132']="invalid variantId of the product with Id ";
$msgcode['133']="customer location does not exist";
$msgcode['134']="access token ,addressId and timeSlot are mandatory";
$msgcode['135']="No default address";
$msgcode['136']="product selected quantity should be greater than zero";
$msgcode['137']="Invalid orderId";
$msgcode['138']="Unable to send OTP";
$msgcode['139']="Hey, It looks like there are some updates in the price, please review your payable amount and proceed to place order.";
$msgcode['140']="Invalid phone number";
$msgcode['141']="Invalid Email";

//Adding new codes - Dhanunjaya
$msgcode['142']="Login mode phone/email is mandatory ";
$msgcode['143']="Access token is mandatory ";
$msgcode['144']="Email is mandatory ";
?>