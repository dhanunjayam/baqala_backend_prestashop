<?php 
include("dbdata.php");
include("msgcode.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);
if(!$_GET['accessToken']) {
  $result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  echo json_encode($result);
  exit(0);
}
else
{
	$access_token=$_GET['accessToken'];
	$sql_custId=mysql_query("SELECT id_customer from ps_customer where access_token = '$access_token' ");
	$num=mysql_num_rows($sql_custId);
  if($num==0)
	{
		$result=array('result'=>array('status'=>0,'message'=>$msgcode[117],'version'=>"1.0",'msgcode'=>117));
  		echo json_encode($result);	
	}
	else
	{

/*    SELECT id_shop FROM `ps_zone_shop` where id_zone=(
SELECT id_zone FROM `ps_zone` where name=(select location from ps_customer where access_token="k5mzs83YPONFMKVdbEZivL6ftxyo2I4GJU9q1CBrXHuTRWewSQh0glaD7cjpAn97323658963"))
*/     
      //$sql_query = "SELECT id_shop FROM ps_customer where access_token = '$access_token' limit 1";

      $sql_query="SELECT id_shop FROM `ps_zone_shop` where id_zone=(SELECT id_zone FROM `ps_zone` where name=(select location from ps_customer where access_token='$access_token'))";
      $sql_shopDetails=mysql_query("$sql_query");
      $num=mysql_num_rows($sql_shopDetails);
      //print_r("num : ".$num);
      $today_slots_array = array();
      $tomorrow_slots_array = array();

      if ($num == 0) {
        $result=array('result'=>array('status'=>0,'message'=>$msgcode[133],'version'=>"1.0",'msgcode'=>133));
        echo json_encode($result);  
      }
      else{

          while ($row=mysql_fetch_assoc($sql_shopDetails)){


             $offset = get_timezone_offset('Asia/Bahrain', date_default_timezone_get());
            // You can then take $offset and adjust your timestamp.
             $offset_time = 0;
             if ($offset < 0) {
                $offset_time = $offset - $DELIVERY_TIMESLOT_BUFFER;
             }
             else{
              $offset_time = $offset + $DELIVERY_TIMESLOT_BUFFER;
             }
            
            //$offset_time = $offset ;
            //echo "$offset\r\n";
            //echo "$offset_time\r\n";
            $min = $offset_time / 60 ;
            $newtimestamp = null;
            if($min < 0){
              $min *= -1;
              $newtimestamp = strtotime("+ $min minute");  
            }
            else{
              $newtimestamp = strtotime("- $min minute");
            }
            
            $final_time = date('H:i A', $newtimestamp);

            //echo "$final_time\r\n";
            
            // if ($final_time <= date("H:i A", strtotime("2:14 PM"))) {
            //   echo "success";
            // }
            // else{
            //   echo "failure";
            // }

            $shop_id =  $row["id_shop"];

            //update default shop id in ps_customer
            $updatequery="update ps_customer set id_shop='$shop_id' where access_token='$access_token'";
            mysql_query("$updatequery");
            
            $today = date('N');
            $sql_query_today = "SELECT * FROM tc_shop_delivery_slots where shop_id = '$shop_id' AND day = $today and active=1";
            $sql_slotDetails_today=mysql_query("$sql_query_today");

            while ($row_today=mysql_fetch_assoc($sql_slotDetails_today)){
               $start_12_hour_format  = date("g:i A", strtotime($row_today['start']));
               $end_12_hour_format  = date("g:i A", strtotime($row_today['end']));

                $today_slots_array[$row_today['id']] = array('start' => $start_12_hour_format, 'end' => $end_12_hour_format, 'max_limit' => $row_today['max_limit'],'slotId' => $row_today['id'], 'availability' => 1);
            }

            //Modify availability
            foreach ($today_slots_array as $slotId => $slotDetail) {
                 $todaysDate = date("Y-m-d");
                 $sql_query_today_availability = "SELECT * FROM tc_shop_delivery_slots_availability where slot_id = $slotId AND order_date = '$todaysDate'";
                 $sql_slotDetails_today_availability = mysql_query("$sql_query_today_availability");
                 $availabilityFlag = 1;
                 if(mysql_num_rows($sql_slotDetails_today_availability)) {
                    
                    while ($row_today_availabilty =mysql_fetch_assoc($sql_slotDetails_today_availability)){

                      //print_r("slotDetail".$slotDetail['max_limit']."\n");
                     // print_r("row_today_availabilty".$row_today_availabilty['nb_order']."\n");
                      if($row_today_availabilty['nb_order'] >= $slotDetail['max_limit']) {
                        $availabilityFlag = 0;
                      } 
                      else{
                        $availabilityFlag = 1;
                      }
                      

                     if(date("H:i A", strtotime($slotDetail['start'])) <= $final_time){
                        $availabilityFlag = 0;
                      }
                      // else{
                      //   $availabilityFlag = 1;
                      // }
                      
                    }
                 
                    $today_slots_array[$slotId] = array('start' => $slotDetail['start'], 'end' => $slotDetail['end'], 'max_limit' => $slotDetail['max_limit'],'slotId' => $slotDetail['slotId'], 'availability' => $availabilityFlag);
                 }
                 else{
                 // print_r("New".date("H:i A", strtotime($slotDetail['start']))."\n");
                     if(date("H:i A", strtotime($slotDetail['start'])) <= $final_time){
                        $availabilityFlag = 0;
                      }
                      $today_slots_array[$slotId] = array('start' => $slotDetail['start'], 'end' => $slotDetail['end'], 'max_limit' => $slotDetail['max_limit'],'slotId' => $slotDetail['slotId'], 'availability' => $availabilityFlag);

                 }
            }

            $tomorrow = null;
            if ($today == 7) {
              $tomorrow = 1;
            }
            else{
              $tomorrow = $today + 1;
            }
            $sql_query_tomorrow = "SELECT * FROM tc_shop_delivery_slots where shop_id = '$shop_id' AND day = $tomorrow and active=1";

            $sql_slotDetails_tomorrow=mysql_query("$sql_query_tomorrow");

            while ($row_tomorrow=mysql_fetch_assoc($sql_slotDetails_tomorrow)){
               $start_tmrw_12_hour_format  = date("g:i A", strtotime($row_tomorrow['start']));
               $end_tmrw_12_hour_format  = date("g:i A", strtotime($row_tomorrow['end']));
                $tomorrow_slots_array[$row_tomorrow['id']] = array('start' => $start_tmrw_12_hour_format, 'end' => $end_tmrw_12_hour_format, 'max_limit' => $row_tomorrow['max_limit'],'slotId' => $row_tomorrow['id'], 'availability' => 1);
            }

            //Modify availability
            foreach ($tomorrow_slots_array as $slotId => $slotDetail) {
                 $tomorrowsDate = date("Y-m-d",strtotime("tomorrow"));
                 $sql_query_tomorrow_availability = "SELECT * FROM tc_shop_delivery_slots_availability where slot_id = $slotId AND order_date = '$tomorrowsDate'";
                 $sql_slotDetails_tomorrow_availability = mysql_query("$sql_query_tomorrow_availability");
                 if(mysql_num_rows($sql_slotDetails_tomorrow_availability)) {
                    $availabilityFlag = 1;
                    while ($row_tomorrow_availabilty =mysql_fetch_assoc($sql_slotDetails_tomorrow_availability)){
                      if($row_tomorrow_availabilty['nb_order'] >= $slotDetail['max_limit']) {
                        $availabilityFlag = 0;
                        //$sql_update_slot=mysql_query("UPDATE `tc_shop_delivery_slots_availability` set nb_order=0 where slot_id=$slotId");
                      }
                    }
                 
                    $tomorrow_slots_array[$slotId] = array('start' => $slotDetail['start'], 'end' => $slotDetail['end'], 'max_limit' => $slotDetail['max_limit'],'slotId' => $slotDetail['slotId'], 'availability' => $availabilityFlag);
                 }
            }
          }       
      }

      $timeSlotsArray = array('today' => array_values($today_slots_array), 'tomorrow' => array_values($tomorrow_slots_array));
      $result=array() ;
      $result=array('result'=> array('status' => 1,'message' =>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'shop_id'=> $shop_id,'timeSlots'=>$timeSlotsArray);
      echo json_encode($result); 
	}
}	

function get_timezone_offset($remote_tz, $origin_tz = null) {
 if($origin_tz === null) {
 if(!is_string($origin_tz = date_default_timezone_get())) {
 return false; // A UTC timestamp was returned — bail out!
 }
 }
 $origin_dtz = new DateTimeZone($origin_tz);
 $remote_dtz = new DateTimeZone($remote_tz);
 $origin_dt = new DateTime("now", $origin_dtz);
 $remote_dt = new DateTime("now", $remote_dtz);
 $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
 return $offset;
}

?>