<?php 
function getLatnLong($address){

	// print_r($address."\n");
			// Get JSON results from this request
	$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key=AIzaSyAhey63zl2FqfEzOIa547bPGUUWOPg4mEY');
	// print_r($geo."\n");
	// Convert the JSON to an array
	$geo = json_decode($geo, true);

	// print_r("Status".$geo['status'].'\n');
	$latLong = array();

	if ($geo['status'] == 'OK') {
	  // Get Lat & Long
	  $latLong['latitude'] = $geo['results'][0]['geometry']['location']['lat'];
	  $latLong['longitude'] = $geo['results'][0]['geometry']['location']['lng'];
	  $latLong['status'] = 'yes';
	}
	else{
	  $latLong['status'] = 'no';	
	}
	return $latLong;
}

function GetDrivingDistance($lat1, $long1,$lat2, $long2)
{
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat2.",".$long2."&destinations=".$lat1.",".$long1."&mode=driving&language=pl-PL&key=AIzaSyAhey63zl2FqfEzOIa547bPGUUWOPg4mEY";

    error_log($url, 0);

    // print_r("+++".$url."+++"); 
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
    //$time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    // return array('distance' => $dist, 'time' => $time);
    return $dist;
}

?>