<?php 
include("dbdata.php");
include("msgcode.php");
error_reporting(E_ERROR);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);

if(!$_GET['query']) {
  $result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  echo json_encode($result);
  exit(0);
}
$search_string=$_GET['query'];
if(!$conn)
{
  $result=array('result'=>array('status'=>0,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  echo json_encode($result);
}
 //select all products for a particular category
else 
{
   $sql='SELECT id_product,name,description FROM `ps_product_lang` where id_lang=1 and id_shop=1 AND name like "%'.$search_string.'%" ';
   $res1=mysql_query($sql);
   echo json_encode($res1);
   $num_rows=mysql_num_rows($res1);
   if($num_rows==0) 
      {
          $result=array('result'=>array('status'=>0,'message'=>$msgcode[111]."'$search_string'".".",'version'=>"1.0",'msgcode'=>111));
          echo json_encode($result);
      }
    else 
      {
          $products = array();
          while($product_row = mysql_fetch_assoc($res1))
             {
              $product['id']=$product_row['id_product'];
              $product['name']=ucwords(strtolower($product_row['name']));
              $product['description']=$product_row['description'];

              //Fetching id_supplier for wholesale 
              //If id_supplier containt other than '0' then we will not show the products to the user
              $wholesale_sql='SELECT id_supplier FROM `ps_product` where id_product='.$product_row['id_product'];
              $wholesale_res=mysql_query($wholesale_sql);
              $wholesale_row=mysql_fetch_assoc($wholesale_res);
              $supplier_id = $wholesale_row['id_supplier'];

              $sql='SELECT id_category_default FROM `ps_product` where id_product='.$product['id'];
              $res=mysql_query($sql);
              $row=mysql_fetch_assoc($res);
              $product['categoryId']=$row['id_category_default'];

              $sql='SELECT * FROM ps_product_attribute WHERE id_product='.$product['id'];
              $res=mysql_query($sql);
              $num_rows=mysql_num_rows($res);
      
              if($num_rows<1)//to fetch product details without any variance
                {
                 $product['variance']="no";
                 $product['variantId']=0;
                 $sql=mysql_query('SELECT quantity FROM ps_stock_available where id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $product['stock']=$row['quantity'];

                 $sql=mysql_query('SELECT price FROM ps_product where id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $product['actualPrice']=$row['price'];

                 $sql=mysql_query('SELECT reduction FROM ps_specific_price where id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $discountAmount=$row['reduction'];
                 if($discountAmount>0){
                  if ($discountAmount == $product['actualPrice']) {
                       $product['discountPrice']= "";
                     }
                     else{
                       $product['discountPrice']="BD ".number_format(($product['actualPrice']-$discountAmount),3);                    
                     }
                   // $product['discountPrice']="BD ".number_format(($product['actualPrice']-$discountAmount),3);
                 }
                 else{
                    $product['discountPrice']="";
                 }
                 $product['actualPrice']="BD ".number_format($product['actualPrice'],3);
                 
                 //id_feature=4 for weight
                 $sql=mysql_query('SELECT id_feature_value FROM `ps_feature_product` where id_feature=4 and id_product='.$product['id']);
                 $row=mysql_fetch_assoc($sql);
                 $id_feature=$row['id_feature_value'];

                 $sql=mysql_query('SELECT value FROM `ps_feature_value_lang` where id_feature_value='.$id_feature);
                 $row=mysql_fetch_assoc($sql);
                 //$product['measure']=$row['value']; 
                 $product['measure']=$row['value']; 
                 $product['measure'] = $product['measure']==null?'':$product['measure'];

                 $sql = mysql_query('SELECT id_image FROM ps_image where id_product='.$product['id'].' and cover=1'); 
                 $num_rows=mysql_num_rows($sql);
                 if($num_rows==0) {
                    $product['image']=$hostURL."/img/p/en.jpg";
                   } 
                 else
                 {    
                   $product['image']=$hostURL."/img/p/";          
                   while($row=mysql_fetch_assoc($sql)) 
                    {
                      $arr = str_split($row['id_image'],1);
                      foreach($arr as $a)
                      {
                        $product['image']=$product['image'].$a.'/';
                      }
                      $product['image']=$product['image'].$row['id_image'].'.jpg';
                    }
                 }

                      $sql= mysql_query('SELECT on_sale, active FROM ps_product where id_product='.$product['id']);

                      $product_row=mysql_fetch_assoc($sql);
                      // print_r($sql);
                      //For Onsale and active - Dhanunjaya
                       $onSale = $product_row['on_sale'];
                      $active = $product_row['active'];
                      $product['onSale']=$onSale; 
                      $product['active']=$active;

                      //Checking for supplier ID - Dhanunjaya
                      if ($supplier_id == 0) {
                        array_push($products,$product);
                      }
                
                      $product=[];
               }// fetched product details without variants
              else
                {
                 $product['variance']="yes";
                 
                  // query to select all the product variants (attribute) 
                 $sql='SELECT id_product_attribute FROM ps_product_attribute where id_product='.$product['id'];
                 $res_at_id=mysql_query($sql);
                 $variants=array();
                 while($row1=mysql_fetch_assoc($res_at_id))
                 {
                   $variant['attributeId']=$row1['id_product_attribute'];
                 /*based on the  product attribute id, we will fetch quantity for the product*/
                   $sql=mysql_query('SELECT quantity FROM ps_stock_available where id_product='.$product['id'].' and id_product_attribute='.$variant['attributeId']);
                   $row=mysql_fetch_assoc($sql);
                   $variant['stock']=$row['quantity'];
                   $sql=mysql_query('SELECT id_image FROM `ps_product_attribute_image` where id_product_attribute='.$variant['attributeId'].' ORDER BY id_image DESC LIMIT 1');
                   $num_rows=mysql_num_rows($sql);
                   if($num_rows==0) 
                   {
                      $variant['image']=$hostURL."/img/p/en.jpg";
                     } 
                   else{  
                      $variant['image']=$hostURL."/img/p/";
                      while($row=mysql_fetch_assoc($sql))
                      {
                        $arr = str_split($row['id_image'],1);
                        foreach($arr as $a)
                          {
                          $variant['image']=$variant['image'].$a.'/';
                          }
                        $variant['image']=$variant['image'].$row['id_image'].'.jpg';
                      }
                   } 
                   $sql='SELECT id_attribute FROM ps_product_attribute_combination where id_product_attribute='.$variant['attributeId'];
                   $res_attributes=mysql_query($sql);

                   while($row2=mysql_fetch_assoc($res_attributes))
                     {
                      $id_attribute=$row2['id_attribute'];
                      $sql='SELECT id_attribute_group FROM ps_attribute where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $id_attribute_group=$row['id_attribute_group'];
                      $sql='SELECT name FROM ps_attribute_group_lang where id_attribute_group='.$id_attribute_group;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $attri_name=$row['name'];

                      if ($row['name'] == 'measure') {
                        $attribute_name = 'measure';
                      }
                      else{
                        $attribute_name = '';
                      }
                      $sql='SELECT name FROM `ps_attribute_lang` where id_attribute='.$id_attribute;
                      $res=mysql_query($sql);
                      $row=mysql_fetch_assoc($res);
                      $variant[$attri_name]=$row['name'];

                      if ($attribute_name == 'measure') {
                        $sep_str = explode(" ", $row['name']);
                        if (strtolower($sep_str[1]) == "kg") {
                          $gm_value = $sep_str[0] * 1000; 
                          $variant['convertedMeasure'] = $gm_value;
                        }
                        else if(strtolower($sep_str[1]) == "ltr"){
                           $ml_value = $sep_str[0] * 1000; 
                          $variant['convertedMeasure'] = $ml_value;
                        }
                        else{
                          $variant['convertedMeasure'] = $sep_str[0];
                        }

                      }

                      }
                     
                      if(!$variant['discountPrice']){
                        $variant['discountPrice']="";
                      }
                    else 
                    {
                        $variant['discountPrice']=number_format($variant['discountPrice'],3);
                        $variant['discountPrice']="BD ".$variant['discountPrice'];
                     }
                        
                        $variant['actualPrice']=number_format($variant['actualPrice'],3);
                        $variant['actualPrice']="BD ".$variant['actualPrice'];
                      
                      //Checking for supplier ID - Dhanunjaya
                      if ($supplier_id == 0) {
                         array_push($variants,$variant);
                       } 
                    
                    $variant=[];
              }

              $sort_col = array();
              foreach ($variants as $key=> $row) {
                  $sort_col[$key] = $row['convertedMeasure'];
              }
               // //For Onsale and active - Dhanunjaya
               //  $onSale = $product_row['on_sale'];
               //  $active = $product_row['active'];
               //  $product['onSale']=$onSale; 
               //  $product['active']=$active;

              array_multisort($sort_col, SORT_ASC, $variants);
              $product=array(
                  'id'=>$product['id'],
                  'name'=>$product['name'],
                  'description'=>$product['description'],
                  'image'=>$variants[0]['image'],
                  'variance'=>$product['variance'],
                  'variantId'=>$variants[0]['attributeId'],
                  'stock'=>$variants[0]['stock'],
                  'measure'=>$variants[0]['measure'],
                  'actualPrice'=>$variants[0]['actualPrice'],
                  'discountPrice'=>$variants[0]['discountPrice'],
                  'variantsList'=>$variants
                    );
                    
                  array_push($products,$product);
                  $product = [];

              }//fetched product variance details 
        }//each product detail along with its variant
      usort($products, function ($elem1, $elem2) {
                 return strcmp($elem1['name'], $elem2['name']);
       });
      $result=array('result'=> array('status' => 1,'message' =>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'searchResult'=>$products);
      echo json_encode($result);  
    }
 }