<?php 
include("dbdata.php");
include("msgcode.php");
include("googlemapsapi.php");

error_reporting(E_ERROR);
header('Content-Type: application/json');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);

if(!$conn)
{
	$result=array('result'=>array('status'=>2,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  	echo json_encode($result);
}
else 
{
	if(!$_GET['accessToken']) 
	{
  		$result=array('result'=>array('status'=>0,'message'=>$msgcode[107],'version'=>"1.0",'msgcode'=>107));
  		echo json_encode($result);
  		exit(0);
	}
	else
	{
		$access_token=$_GET['accessToken'];
		$sql_custId=mysql_query("SELECT id_customer from ps_customer where access_token = '$access_token' ");
		$num=mysql_num_rows($sql_custId);
		if($num==0)
			{
			$result=array('result'=>array('status'=>0,'message'=>$msgcode[117],'version'=>"1.0",'msgcode'=>117));
  			echo json_encode($result);	
			}
		else
			{
			$res_custId=mysql_fetch_assoc($sql_custId);
			$customerId=$res_custId['id_customer'];
			//print_r($customerId);
			$sql_numadd=mysql_query("SELECT id_address from ps_address where id_customer=$customerId");
			$num_add=mysql_num_rows($sql_numadd);
			if($num_add==0)
			{
				$result=array('result'=>array('status'=>0,'message'=>$msgcode[129],'version'=>"1.0",'msgcode'=>129));
  				echo json_encode($result);		
			}
			else
			{
				if(!$_GET['addressId'])
					{
					$sql_defalutaddress=mysql_query("SELECT id_address,city,address1,address2,address_type,landmark,latitude, longitude FROM `ps_address` where id_customer=$customerId AND default_add=1");
					if(!$sql_defalutaddress)
						die("invalid query1".mysql_error());
					$num=mysql_num_rows($sql_defalutaddress);
					if($num==0) 
						{
						$addresses=array();
						$sql_address=mysql_query("SELECT id_address,city,address1,address2,address_type,landmark,latitude, longitude FROM `ps_address` where id_customer=$customerId and default_add=0");
						if(!$sql_address)
							die("invalid address".mysql_error());
						else
							{	
							while($row=mysql_fetch_assoc($sql_address))
								{
								//print_r("inside while");	
								$address['id_address']=$row['id_address'];
								$address['location']=$row['city'];
								$address['address1']=$row['address1'];
								$address['address2']=$row['address2'];
								$address['addressType']=$row['address_type'];
								$address['landmark']=$row['landmark'];
								$address['latitude']=$row['latitude'];
								$address['longitude']=$row['longitude'];

								array_push($addresses,$address);
								}
							$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'addresses'=>$addresses);
							echo json_encode($result);
							}
						} 
					else 
						{	
						$addresses=array();
						$row=mysql_fetch_assoc($sql_defalutaddress);
						$address['id_address']=$row['id_address'];
						$address['location']=$row['city'];
						$address['address1']=$row['address1'];
						$address['address2']=$row['address2'];
						$address['addressType']=$row['address_type'];
						$address['landmark']=$row['landmark'];
						$address['latitude']=$row['latitude'];
						$address['longitude']=$row['longitude'];
						array_push($addresses,$address);
						$sql_address=mysql_query("SELECT id_address,city,address1,address2,address_type,landmark,latitude, longitude FROM `ps_address` where id_customer=$customerId and default_add=0");
						if(!$sql_address)
							die("invalid address".mysql_error());
						else
							{	
							while($row=mysql_fetch_assoc($sql_address))
								{	
								$address['id_address']=$row['id_address'];
								$address['location']=$row['city'];
								$address['address1']=$row['address1'];
								$address['address2']=$row['address2'];
								$address['addressType']=$row['address_type'];
								$address['landmark']=$row['landmark'];
								$address['latitude']=$row['latitude'];
								$address['longitude']=$row['longitude'];
								array_push($addresses,$address);
								}
							$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'addresses'=>$addresses);
							echo json_encode($result);
							}
						}
					}
				else
					{
					$newdefalutAdd=$_GET['addressId'];
					$addressFromApp = mysql_query("SELECT * from ps_address where id_address=$newdefalutAdd");
					$addressFromApp_row=mysql_fetch_assoc($addressFromApp);
					// $defaultAddress=$addressFromApp_row['id_address'];
					$latitude=$addressFromApp_row['latitude'];
					$longitude=$addressFromApp_row['longitude'];
					// print_r($latitude);
					//Checking for latitude and longitude
					//If both are 0, need to send the address to Google maps API
					//Otherwise, send lat and long to mobile app
					if ($latitude == 0.000000 && $longitude == 0.000000) 
					{
						$fullAddress =  $addressFromApp_row['address1'] ." ". $addressFromApp_row['address2'] ." ". $addressFromApp_row['city'];
						$latLong = getLatnLong($fullAddress);
						$latLongStatus = $latLong['status'];
						// print_r("latLong++++".$latLong['status'].'\n');
						if ($latLongStatus == 'no') {
							$result=array('result'=>array('status'=>0,'message'=>$msgcode[146],'version'=>"1.0",'msgcode'=>146,'addressId'=>$addressFromApp_row['id_address']));
					  		echo json_encode($result);
					  		exit(0);
						}
						else{
							// $address['latitude']=$latLong['latitude'];
							// $address['longitude']=$latLong['longitude'];
							$new_latitude=$latLong['latitude'];
							$new_longitude=$latLong['longitude'];
							$addressId = $addressFromApp_row['id_address'];
							$sql_address=mysql_query("UPDATE `ps_address` set latitude=$new_latitude, longitude=$new_longitude where id_address=$addressId");

						}
					}
					// else
					// {
					// 	$address['latitude']=$latitude;
					// 	$address['longitude']=$longitude;
					// }



					// print_r("given Id :".$newdefalutAdd);
					$sql_defalutaddress=mysql_query("SELECT id_address FROM `ps_address` where id_customer=$customerId AND default_add=1");
					$row=mysql_fetch_assoc($sql_defalutaddress);
					$defaultAddress=$row['id_address'];

					//print_r("default add: ".$defaultAddress);
					//$sql=mysql_query("UPDATE ps_address set default_add=0 where id_address=$defaultAddress");

					$sql1=mysql_query("UPDATE ps_address set default_add=1 where id_address=$newdefalutAdd");
						//echo 'count'. mysql_num_rows($sql);	
					$selecte_SQL = mysql_query("SELECT * from ps_address where default_add=1 AND id_address=$newdefalutAdd");
						//echo 'count'.mysql_num_rows($selecte_SQL);
								
					if(mysql_num_rows($selecte_SQL)){
						$sql=mysql_query("UPDATE ps_address set default_add=0 where id_address=$defaultAddress");
					}
					//$sql=mysql_query("UPDATE ps_address set default_add=0 where id_address=$defaultAddress");

					$sql_defalutaddress=mysql_query("SELECT id_address,city,address1,address2,address_type, latitude, longitude,landmark FROM `ps_address` where id_customer=$customerId AND default_add=1");
					// print_r($sql_defalutaddress);
					if(!$sql_defalutaddress)
						die("invalid query1".mysql_error());
					else
						{	
							$addresses=array();
							while($row=mysql_fetch_assoc($sql_defalutaddress))
							{
								$address['id_address']=$row['id_address'];
								$address['location']=$row['city'];
								$address['address1']=$row['address1'];
								$address['address2']=$row['address2'];
								$address['addressType']=$row['address_type'];
								$address['landmark']=$row['landmark'];
								$address['latitude']=$row['latitude'];;
								$address['longitude']=$row['longitude'];;
								array_push($addresses,$address);
							}
						}
						$location=mysql_real_escape_string($address['location']);
						$sql_updatecustLocation=mysql_query("UPDATE`ps_customer` set location='$location' where id_customer=$customerId");
					$sql_address=mysql_query("SELECT id_address,city,address1,address2,address_type, landmark,latitude, longitude FROM `ps_address` where id_customer=$customerId and default_add=0");
					if(!$sql_address)
						die("invalid address".mysql_error());
					else
						{	
						while($row=mysql_fetch_assoc($sql_address))
						{	
							$address['id_address']=$row['id_address'];
							$address['location']=$row['city'];
							$address['address1']=$row['address1'];
							$address['address2']=$row['address2'];
							$address['addressType']=$row['address_type'];
							$address['landmark']=$row['landmark'];
							$address['latitude']=$row['latitude'];;
							$address['longitude']=$row['longitude'];;
							array_push($addresses,$address);
						}
						$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'addresses'=>$addresses);
						echo json_encode($result);
						}
					}
				}
			}	
		}
	}
?>