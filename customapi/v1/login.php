<?php 
include("dbdata.php");
include("msgcode.php");
include("smsgateway.php");
include("functions.php");
include_once("defaultTimeZone.php");

error_reporting(E_ERROR);
header('Content-Type: application/json');
//header('Content-Type: application/form-data');
$conn=mysql_connect($servername,$uname,$pwd);
mysql_select_db($dbname);

if(!$conn)
{
	$result=array('result'=>array('status'=>2,'message'=>$msgcode[101],'version'=>"1.0",'msgcode'=>101));
  	echo json_encode($result);
}
else 
{
	if(!$_GET['loginMode']) 
	{
		$result=array('result'=>array('status'=>0,'message'=>$msgcode[142],'version'=>"1.0",'msgcode'=>142));
  		echo json_encode($result);
  		exit(0);
	}
	else{
		$location = $_GET["location"];		
		$login_mode = $_GET['loginMode'];

		//Check for phone no. login mode
		if ($login_mode == 'phone') {

			//Phone No. is mandatory when login mode is phone
			if(!$_GET['phNo']) {
				$result=array('result'=>array('status'=>0,'message'=>$msgcode[115],'version'=>"1.0",'msgcode'=>115));
  				echo json_encode($result);
  				exit(0);
			}
			else{
				$ph_no = substr($_GET['phNo'], 1);

				//Access Token is Mandatory
				if (!$_GET['accessToken']) {
					$result=array('result'=>array('status'=>0,'message'=>$msgcode[143],'version'=>"1.0",'msgcode'=>143));
  					echo json_encode($result);
  					exit(0);
				}
				else{
					$access_token = $_GET['accessToken'];
					$customer_sql=mysql_query("select id_customer from ps_customer where phone_num=$ph_no");
					$num_rows_customer=mysql_num_rows($customer_sql);
					if ($num_rows_customer == 0) {
						//Phone number is not available in DB, so we can insert a new row
						$sql = "INSERT INTO `ps_customer`(id_default_group,location,phone_num,time_stamp, access_token) values (2 , '$location', '$ph_no', NOW(), '$access_token')";
						$sql_query=mysql_query($sql);

						//Get cart id
						$cartId = getCartId($ph_no,'');
						$newPhone = '+'.$ph_no;
						$userDetails=array('phoneNumber'=>$newPhone, 'cartId'=>$cartId, 'accessToken' => $access_token,'location'=>$location,'isPhoneNoVerified' => true);
						$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'userDetails'=>$userDetails);
  						echo json_encode($result);
					}
					else{
						//Phone number already exists, so we can update accessToken and other information
						$sql=mysql_query("UPDATE ps_customer set access_token = '$access_token',time_stamp = NOW() where phone_num=$ph_no");

						//Get cart id
						$cartId = getCartId($ph_no,'');
						$newPhone = '+'.$ph_no;
						$userDetails=array('phoneNumber'=>$newPhone, 'cartId'=>$cartId, 'accessToken' => $access_token,'location'=>$location,'isPhoneNoVerified' => true);
						$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200),'userDetails'=>$userDetails);
  						echo json_encode($result);
					}
				}
			}
		}
		else{ // Check for email login mode
			
			//Email is mandatory when login mode is Email
			if (!$_GET['emailId']) {
				$result=array('result'=>array('status'=>0,'message'=>$msgcode[144],'version'=>"1.0",'msgcode'=>144));
  				echo json_encode($result);
  				exit(0);
			}
			else{
				$email_id = $_GET['emailId'];
				
				//Access Token is Mandatory
				if (!$_GET['accessToken']) {
					$result=array('result'=>array('status'=>0,'message'=>$msgcode[143],'version'=>"1.0",'msgcode'=>143));
  					echo json_encode($result);
  					exit(0);
				}
				else{
					$isPhoneNoVerified = false;
					$firstName = $_GET['firstName'];
					$lastName = $_GET['lastName'];
					$access_token = $_GET['accessToken'];
					$customer_sql=mysql_query("select phone_num from ps_customer where email= '$email_id'");
					$customer_sql_row=mysql_fetch_assoc($customer_sql);
					$phoneNo = $customer_sql_row['phone_num'];
					//print_r("PhNoss:::".$customer_sql_row['phone_num']);
					if ($phoneNo != '') {
						$isPhoneNoVerified = true;
					}
					$num_rows_customer=mysql_num_rows($customer_sql);
					if ($num_rows_customer == 0) {
						//Email is not available in DB, so we can insert a new row
						$sql = "INSERT INTO `ps_customer`(id_default_group,location,email,time_stamp, access_token, firstname, lastname) values (2 , '$location', '$email_id', NOW(), '$access_token', '$firstName', '$lastName')";
						$sql_query=mysql_query($sql);

						//Get cart id
						$cartId = getCartId('',$email_id);
						$userDetails=array('phoneNumber'=>$phoneNo, 'cartId'=>$cartId, 'accessToken' => $access_token, 'firstName'=>$firstName, 'lastName'=>$lastName, 'email'=> $email_id, 'location'=>$location,'isPhoneNoVerified' => $isPhoneNoVerified);
						$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200), 'userDetails'=>$userDetails);
  						echo json_encode($result);
					}
					else{
						//Email already exists, so we can update accessToken and other information
						$sql=mysql_query("UPDATE ps_customer set access_token = '$access_token',time_stamp = NOW(), firstname = '$firstName', lastname = '$lastName' where email= '$email_id'");

						//Get cart id
						$cartId = getCartId('',$email_id);
						$userDetails=array('phoneNumber'=>$phoneNo, 'cartId'=>$cartId, 'accessToken' => $access_token, 'firstName'=>$firstName, 'lastName'=>$lastName,'email'=> $email_id, 'location'=>$location,'isPhoneNoVerified' => $isPhoneNoVerified);
						$result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200), 'userDetails'=>$userDetails);

						// $result=array('result'=>array('status'=>1,'message'=>$msgcode[200],'version'=>"1.0",'msgcode'=>200, 'isPhoneNoVerified' => $isPhoneNoVerified));
  						echo json_encode($result);
					}
				}
			}
		}
	}
}


function getCartId($phoneNo, $email){

	$customerId = '';
	$num_rows_customer = 0;
	if ($phoneNo == "") {
		//Get id_customer using email
		// print_r('SELECT id_customer FROM `ps_customer` where email=$email');

		$customer_id_sql=mysql_query("SELECT id_customer FROM `ps_customer` where email= '$email'");
		$customer_id_sql_row=mysql_fetch_assoc($customer_id_sql);
		$num_rows_customer=mysql_num_rows($customer_id_sql);
		$customerId=$customer_id_sql_row['id_customer'];
	}
	else{
		//Get id_customer using phone
		$customer_id_sql=mysql_query("SELECT id_customer FROM `ps_customer` where phone_num=$phoneNo");
		$customer_id_sql_row=mysql_fetch_assoc($customer_id_sql);
		
		$customerId=$customer_id_sql_row['id_customer'];	
	}

	$id_cart_sql=mysql_query("SELECT id_cart from ps_cart where id_customer=$customerId ORDER BY id_cart DESC limit 1");
	$id_cart_sql_row=mysql_fetch_assoc($id_cart_sql);
	$num_rows_cart=mysql_num_rows($id_cart_sql);
	
	//print_r('INSERT INTO `ps_cart` (id_customer) values ($customerId)');
	if ($num_rows_cart == 0) {
		$current_date = date("Y-m-d H:i:s");
		$ps_cart_sql=mysql_query("INSERT INTO `ps_cart` (id_customer, date_add) values ($customerId, '$current_date')");
		if(!$ps_cart_sql)
			die("invalid query: ".mysql_error());
		$id_cart_sql=mysql_query("SELECT id_cart from ps_cart where id_customer=$customerId ORDER BY id_cart DESC limit 1");
		$id_cart_sql_row=mysql_fetch_assoc($id_cart_sql);
		$cartId=$id_cart_sql_row['id_cart'];
		return $cartId;
	}
	else{
		$cartId=$id_cart_sql_row['id_cart'];
		return $cartId;
	}
	
}	

?>