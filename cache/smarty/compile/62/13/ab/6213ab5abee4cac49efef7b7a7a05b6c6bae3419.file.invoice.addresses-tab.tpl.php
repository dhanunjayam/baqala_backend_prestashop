<?php /* Smarty version Smarty-3.1.19, created on 2017-03-29 11:56:56
         compiled from "/var/www/html/pdf/invoice.addresses-tab.tpl" */ ?>
<?php /*%%SmartyHeaderCode:96799131258db76d8a78b79-40831708%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6213ab5abee4cac49efef7b7a7a05b6c6bae3419' => 
    array (
      0 => '/var/www/html/pdf/invoice.addresses-tab.tpl',
      1 => 1473231013,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '96799131258db76d8a78b79-40831708',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order_invoice' => 0,
    'deliverySlots' => 0,
    'customerInfo' => 0,
    'delivery_address' => 0,
    'invoice_address' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58db76d8a9c3e2_56660929',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58db76d8a9c3e2_56660929')) {function content_58db76d8a9c3e2_56660929($_smarty_tpl) {?>
<table id="addresses-tab" cellspacing="0" cellpadding="0">
	<tr>
		<td width="33%"><span><u>Seller Name</u> </span><br/><br/>
			<!-- <?php if (isset($_smarty_tpl->tpl_vars['order_invoice']->value)) {?><?php echo $_smarty_tpl->tpl_vars['order_invoice']->value->shop_address;?>
<?php }?> -->
			<?php echo $_smarty_tpl->tpl_vars['deliverySlots']->value['shopname'];?>

			<br>
			Delivery Date :<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['deliverySlots']->value['deliveryDate']),$_smarty_tpl);?>
 
			<br>
			Delivery Slot : <?php echo $_smarty_tpl->tpl_vars['deliverySlots']->value['startTime'];?>
 - <?php echo $_smarty_tpl->tpl_vars['deliverySlots']->value['endTime'];?>

		</td>
		<td width="33%" style="text-align:center"><span class="bold"><u>Customer Info</u></span><br/><br/>
			<!-- <?php if (isset($_smarty_tpl->tpl_vars['order_invoice']->value)) {?><?php echo $_smarty_tpl->tpl_vars['order_invoice']->value->shop_address;?>
<?php }?> -->
			<?php echo $_smarty_tpl->tpl_vars['customerInfo']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['customerInfo']->value['lastname'];?>

			<br>
			<?php echo $_smarty_tpl->tpl_vars['customerInfo']->value['email'];?>

			<br>
			<?php echo $_smarty_tpl->tpl_vars['customerInfo']->value['phone_num'];?>

		</td>
		<td width="33%" style="float:right;text-align:right"><?php if ($_smarty_tpl->tpl_vars['delivery_address']->value) {?><span class="bold"><u><?php echo smartyTranslate(array('s'=>'Delivery Address','pdf'=>'true'),$_smarty_tpl);?>
</u></span><br/><br/>
				<?php echo $_smarty_tpl->tpl_vars['delivery_address']->value;?>

			<?php }?>
		</td>
		<!-- <td width="33%"><span class="bold"><?php echo smartyTranslate(array('s'=>'Billing Address','pdf'=>'true'),$_smarty_tpl);?>
</span><br/><br/>
				<?php echo $_smarty_tpl->tpl_vars['invoice_address']->value;?>

		</td> -->
	</tr>
</table>
<?php }} ?>
